#!/bin/bash
###############################################################################
#                               Documentation                                 #
###############################################################################
#                                                                             #
# Description                                                                 #
#     :
#                                                                             #
#                                                                             #
#                                                                             #
###############################################################################
#                           Identify Script Home                              #
###############################################################################

#Find the script file home
pushd . > /dev/null
SCRIPT_DIRECTORY="${BASH_SOURCE[0]}";
while([ -h "${SCRIPT_DIRECTORY}" ]);
do
    cd "`dirname "${SCRIPT_DIRECTORY}"`"
    SCRIPT_DIRECTORY="$(readlink "`basename "${SCRIPT_DIRECTORY}"`")";
done
cd "`dirname "${SCRIPT_DIRECTORY}"`" > /dev/null
SCRIPT_DIRECTORY="`pwd`";
popd  > /dev/null
MODULE_HOME="`dirname "${SCRIPT_DIRECTORY}"`"

fn_assert_variable_is_set "MODULE_NAME" $MODULE_HOME

###############################################################################
#                           Import Dependencies                               #
###############################################################################

#Load common dependencies
. ${MODULE_HOME}/bin/import-dependecies.sh

###############################################################################
#                                CODE                                         #
###############################################################################

while getopts ":t:f:" opt; do
    case $opt in
        t) SYNC_TABLE="$OPTARG"
        ;;
        f) FLOW_NAME="$OPTARG"
        ;;
        *) fn_log_info "Invalid option -$OPTARG"
        ;;
    esac
done

fn_assert_variable_is_set "FLOW_NAME" $FLOW_NAME
fn_assert_variable_is_set "SYNC_TABLE" $SYNC_TABLE

SUCCESS_MESSAGE='Successfully completed schema validation.'
FAILURE_MESSAGE='Failed to complete schema validation, FAILURE IN SCHEMA VALIDATION'
CONFIG_FILE_PATH=${CONFIG_HOME}/${FLOW_NAME}"-"${SYNC_TABLE}"/sync-configurations.xml"

fn_assert_variable_is_set "CONFIG_FILE_PATH" $CONFIG_FILE_PATH

fn_run_java ${APPLICATION_INSTALL_DIR} "com.utility.migrator.shema_validator.SchemaValidator -t ${SYNC_TABLE} -c ${CONFIG_FILE_PATH}"

###############################################################################
#                                     End                                     #
###############################################################################
