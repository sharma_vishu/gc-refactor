#!/bin/bash
###############################################################################
#                               Documentation                                 #
###############################################################################
#                                                                             #
# Description                                                                 #
#     :
#                                                                             #
#                                                                             #
#                                                                             #
###############################################################################
#                           Identify Script Home                              #
###############################################################################
#Find the script file home

pushd . > /dev/null
SCRIPT_DIRECTORY="${BASH_SOURCE[0]}";

while([ -h "${SCRIPT_DIRECTORY}" ]);
do
    cd "`dirname "${SCRIPT_DIRECTORY}"`"
    SCRIPT_DIRECTORY="$(readlink "`basename "${SCRIPT_DIRECTORY}"`")";
done

cd "`dirname "${SCRIPT_DIRECTORY}"`" > /dev/null
SCRIPT_DIRECTORY="`pwd`";
popd  > /dev/null
MODULE_HOME="`dirname "${SCRIPT_DIRECTORY}"`"

fn_assert_variable_is_set "MODULE_NAME" $MODULE_HOME

###############################################################################
#                           Import Dependencies                               #
###############################################################################

#Load common dependencies
. ${MODULE_HOME}/bin/import-dependecies.sh

###############################################################################
#                                CODE                                         #
###############################################################################


while getopts ":t:f:" opt; do
    case $opt in
        t) SYNC_TABLE="$OPTARG"
        ;;
        f) FLOW_NAME="$OPTARG"
        ;;
        *) echo "Invalid option -$OPTARG"
        ;;
    esac
done

fn_assert_variable_is_set "FLOW_NAME" $FLOW_NAME
fn_assert_variable_is_set "SYNC_TABLE" $SYNC_TABLE


CONFIG_FILE_PATH=${CONFIG_HOME}/${FLOW_NAME}"-"${SYNC_TABLE}"/sync-configurations.xml"
fn_assert_variable_is_set "CONFIG_FILE_PATH" $CONFIG_FILE_PATH

#Delete the Hive Based Text & Incremental Table
hive -e "DROP TABLE IF EXISTS ${SYNC_TABLE}_text" &
hive -e "DROP TABLE IF EXISTS ${SYNC_TABLE}_incremental" &
echo "Dropping Hive based Tables" wait
echo "Successfully Completed deleting the Hive based text table and incremental table"

#Cleaning up of the BQ Staging Table
BQ_DATASET=`mysql -h ${HOST} --port 3307 -u ${SQL_USR} -p${SQL_PASS} -e "select bq_schema_name from ${DB_NAME}.bq_dictionary where ddh_table_name='${SYNC_TABLE}' limit 1"`
BQ_DATASET=`echo $BQ_DATASET | cut -d ' ' -f2`
TABLE_NAME=`echo "${SYNC_TABLE}" | awk -F '.' '{print $2}'`
BQ_DATASET=${BQ_DATASET}.${TABLE_NAME}
fn_assert_variable_is_set "BQ_DATASET" $BQ_DATASET

BQ_STAGE_TABLE=${BQ_DATASET}"_stage"
fn_assert_variable_is_set "BQ_STAGE_TABLE" $BQ_STAGE_TABLE

bq rm -f $BQ_STAGE_TABLE

#Update the status of the job
fn_run_java ${APPLICATION_INSTALL_DIR} "com.utility.migrator.sql_utility.JobTaskUpdater -f ${FLOW_NAME} -t ${SYNC_TABLE} -jst "successfully-completed-migrational-transfer" -jss SUCCESSFUL -c ${CONFIG_FILE_PATH}"


#Cleaning Up configuration file path
CONF_FILE_PATH=${CONFIG_HOME}/${FLOW_NAME}"-"${SYNC_TABLE}

fn_assert_variable_is_set "CONF_FILE_PATH" $CONF_FILE_PATH

rm -rf ${CONFIG_HOME}/${FLOW_NAME}"-"${SYNC_TABLE}

EXIT_CODE=`fn_get_exit_code $?`
fn_handle_exit_code_modules "${EXIT_CODE}" "Migrational Task completed successfully." "Failed to persist and/or complete the cleanup tasks-FAILURE IN datasync-migrational-transfer" "${FAIL_ON_ERROR}" "${FLOW_NAME}" "${SYNC_TABLE}"

HIVE_COUNT=$(hive -e "select count (*) from ${SYNC_TABLE}")

fn_assert_variable_is_set "HIVE_COUNT" $HIVE_COUNT

BQ_COUNT=(`bq query --format=csv "select count(*) from ${BQ_DATASET}" | tail -n 1`)

fn_assert_variable_is_set "BQ_COUNT" $BQ_COUNT

if [ $BQ_COUNT -eq $HIVE_COUNT ]
then
    fn_log_info "Count Matched"
else
    fn_log_info "Count didnt match"
fi

##the function to compare the sizes is still to be included here
fn_log_info "BQ_COUNT = $BQ_COUNT"
fn_log_info "HIVE_COUNT = $HIVE_COUNT"
BODY="BQ_COUNT = $BQ_COUNT
HIVE_COUNT = $HIVE_COUNT"

fn_assert_variable_is_set "BODY" $BODY

fn_send_email ${EMAIL_ID} "Google-Cloud DataSync-Flow" "$BODY"

##############################################################################
#                                     End                                     #
###############################################################################
