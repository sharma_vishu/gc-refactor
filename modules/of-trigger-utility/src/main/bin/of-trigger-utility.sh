#!/bin/bash
###############################################################################
#                               Documentation                                 #
###############################################################################
#                                                                             #
# Description                                                                 #
#     :
#                                                                             #
#                                                                             #
#                                                                             #
###############################################################################
#                           Identify Script Home                              #
###############################################################################

#Find the script file home
pushd . > /dev/null
SCRIPT_DIRECTORY="${BASH_SOURCE[0]}";
while([ -h "${SCRIPT_DIRECTORY}" ]);
do
    cd "`dirname "${SCRIPT_DIRECTORY}"`"
    SCRIPT_DIRECTORY="$(readlink "`basename "${SCRIPT_DIRECTORY}"`")";
done
cd "`dirname "${SCRIPT_DIRECTORY}"`" > /dev/null
SCRIPT_DIRECTORY="`pwd`";
popd  > /dev/null
MODULE_HOME="`dirname "${SCRIPT_DIRECTORY}"`"

fn_assert_variable_is_set "MODULE_NAME" $MODULE_HOME

###############################################################################
#                           Import Dependencies                               #
###############################################################################

#Load common dependencies
. ${MODULE_HOME}/bin/import-dependecies.sh

###############################################################################
#                                CODE                                         #
###############################################################################

fn_log_info "I am in Trigger utility shell script"

CONFIG_HOME=${APPLICATION_INSTALL_DIR}/config
fn_assert_variable_is_set "CONFIG_HOME" $CONFIG_HOME

SHARED_LIB=${APPLICATION_INSTALL_DIR}/lib
fn_assert_variable_is_set "SHARED_LIB" $SHARED_LIB

TRIGGER_CONFIG_FILE_PATH=${CONFIG_HOME}"/trigger-configurations.properties"
fn_assert_variable_is_set "TRIGGER_CONFIG_FILE_PATH" $TRIGGER_CONFIG_FILE_PATH

#Trigger Framework Starter Job
fn_run_java ${APPLICATION_INSTALL_DIR} "com.trigger.utilities.TriggerMain -c ${TRIGGER_CONFIG_FILE_PATH}"

###############################################################################
#                                     End                                     #
###############################################################################
