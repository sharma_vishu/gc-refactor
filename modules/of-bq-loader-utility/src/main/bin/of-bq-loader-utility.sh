#!/bin/bash
###############################################################################
#                               Documentation                                 #
###############################################################################
#                                                                             #
# Description                                                                 #
#     :
#                                                                             #
#                                                                             #
#                                                                             #
###############################################################################
#                           Identify Script Home                              #
###############################################################################
#Find the script file home
pushd . > /dev/null
SCRIPT_DIRECTORY="${BASH_SOURCE[0]}";
while([ -h "${SCRIPT_DIRECTORY}" ]);
do
  cd "`dirname "${SCRIPT_DIRECTORY}"`"
  SCRIPT_DIRECTORY="$(readlink "`basename "${SCRIPT_DIRECTORY}"`")";
done
cd "`dirname "${SCRIPT_DIRECTORY}"`" > /dev/null
SCRIPT_DIRECTORY="`pwd`";
popd  > /dev/null
MODULE_HOME="`dirname "${SCRIPT_DIRECTORY}"`"

fn_assert_variable_is_set "MODULE_NAME" $MODULE_HOME

###############################################################################
#                           Import Dependencies                               #
###############################################################################
#Load common dependencies
. ${MODULE_HOME}/bin/import-dependecies.sh
###############################################################################
#                                CODE                                         #
###############################################################################
while getopts ":t:f:" opt; do
    case $opt in
        t) SYNC_TABLE="$OPTARG"
        ;;
        f) FLOW_NAME="$OPTARG"
        ;;
        *) fn_log_info "Invalid option -$OPTARG"
        ;;
    esac
done

fn_assert_variable_is_set "FLOW_NAME" $FLOW_NAME
fn_assert_variable_is_set "SYNC_TABLE" $SYNC_TABLE

###############################################################################

CONFIG_FILE_PATH=${CONFIG_HOME}/${FLOW_NAME}"-"${SYNC_TABLE}"/sync-configurations.xml"
fn_assert_variable_is_set "CONFIG_FILE_PATH" $CONFIG_FILE_PATH

#BQ_DATASET=$(fn_run_java ${APPLICATION_INSTALL_DIR} "com.utility.migrator.loader_utility.FetchBQName -t ${SYNC_TABLE} -c ${CONFIG_FILE_PATH})"
#fn_assert_variable_is_set "BQ_DATASET" $BQ_DATASET


BQ_DATASET=`mysql -h ${HOST} --port 3307 -u ${SQL_USR} -p${SQL_PASS} -e "select bq_schema_name from ${DB_NAME}.bq_dictionary where ddh_table_name='${SYNC_TABLE}' limit 1"`
BQ_DATASET=`echo $BQ_DATASET | cut -d ' ' -f2`

TABLE_NAME=`echo "${SYNC_TABLE}" | awk -F '.' '{print $2}'`
BQ_DATASET=${BQ_DATASET}.${TABLE_NAME}

BQ_STAGE_TABLE=${BQ_DATASET}"_stage"
fn_assert_variable_is_set "BQ_STAGE_TABLE" $BQ_STAGE_TABLE

HIVE_TEXT_TABLE_NAME=${SYNC_TABLE}"_text"
fn_assert_variable_is_set "HIVE_TEXT_TABLE_NAME" $HIVE_TEXT_TABLE_NAME

declare -a partition_error_record=();

#Code to check if the Avro based Hive Table exists
hive -e "describe ${HIVE_TEXT_TABLE_NAME}" >> /dev/null
err_code=`echo "$?"`
if [ $err_code -ne 0 ]
then
    fn_log_info "Text Formatted Hive Table doesn't exists ! "
    fn_handle_exit_code_modules "${err_code}" "Text Formatted Hive Table  exists " "Text Formatted Hive Table doesn't exists FAILED IN BQ-LOADER-UTILITY" "${FAIL_ON_ERROR}" "${FLOW_NAME}" "${SYNC_TABLE}"
fi

fn_log_info "BQ_DATASET=${BQ_DATASET}"
  # Creating back-up table of the principal bigquery table
bq show ${BQ_DATASET} >>/dev/null
if [ $? -eq 0 ]
 then
   bq rm -f ${BQ_DATASET}_backup
   bq cp ${BQ_DATASET} ${BQ_DATASET}_backup
   if [ $? -ne 0 ]
    then
        fn_log_info "Big Query backup operation failed ...."
        exit 1
   fi
fi

bq mk --time_partitioning_type=DAY ${BQ_DATASET}_stage_partition
partition_array=(`hive -S -e "show partitions ${HIVE_TEXT_TABLE_NAME};"`)
gs_location=`hive -S -e "describe formatted  ${HIVE_TEXT_TABLE_NAME};" | grep 'Location'| awk '{ print $NF }'`
fn_log_info "Google Storage Location for the text table Loader file is ${gs_location}"

partition_array=(`hive -S -e "show partitions ${HIVE_TEXT_TABLE_NAME};"`)
fn_assert_variable_is_set "partition_array" $partition_array

gs_location=`hive -S -e "describe formatted  ${HIVE_TEXT_TABLE_NAME};" | grep 'Location'| awk '{ print $NF }'`
fn_assert_variable_is_set "gs_location" $gs_location

fn_log_info "Google Storage Location for the text table Loader file is ${gs_location}"

count=`hive -e "select count(*) from ${HIVE_TEXT_TABLE_NAME}"`
ingestion_time=`date +%Y%m%d`
fn_assert_variable_is_set "ingestion_time" $ingestion_time

part_id=`fn_log_info ${partition_array[0]} | awk -F '=' '{ print $2 }'`
fn_assert_variable_is_set "part_id" $part_id

partitioned=`mysql -h ${HOST} --port 3307 -u ${SQL_USR} -p${SQL_PASS} -e "select partitioned from ${DB_NAME}.bq_partitioned_data where table_name='${SYNC_TABLE}' limit 1"`
partitioned=`fn_log_info $partitioned | cut -d ' ' -f2`

if [ ${count} -ne 0 ]
then
    if [ ${#partition_array[@]} -eq 1 ] && [ "$partitioned" == "no" ]
    then
        fn_log_info "non-partitioned table "
        bq cp --append_table ${BQ_DATASET}  ${BQ_DATASET}_stage_partition
        bq rm -f ${BQ_DATASET}
    else
        fn_log_info "partitioned table"
        for  PARTITIONS in "${partition_array[@]}"
        do
            partition_id=`fn_log_info ${PARTITIONS} | awk -F '=' '{ print $2 }'`
            fn_assert_variable_is_set "partition_id" $partition_id

            QUERY=${BQ_DATASET}'$'${partition_id}
            fn_assert_variable_is_set "QUERY" $QUERY

            QUERY_STAGE=${BQ_DATASET}_stage_partition'$'${partition_id}
            fn_assert_variable_is_set "QUERY_STAGE" $QUERY_STAGE

            bq query --allow_large_results --append_table --noflatten_results --destination_table ${QUERY_STAGE} 'select * from ${QUERY}'>>/dev/null
            exit_value=`echo "$?"`
            if [ ${exit_value} -ne 0 ]
            then
                fn_log_info "failed to copy modified partitions into $BQ_DATASET_stage_partition !!!!"
                fn_handle_exit_code_modules "${exit_value}" "" "failed to copy modified partitions into $BQ_DATASET_stage_partition !!!! FAILED IN BQ-LOADER-UTILITY" "${FAIL_ON_ERROR}" "${FLOW_NAME}" "${SYNC_TABLE}"
            else
                bq rm -f ${QUERY}
            fi
        done
    fi
fi
for PARTITIONS in "${partition_array[@]}"
do
    partition_id=`echo ${PARTITIONS} | awk -F '=' '{ print $2 }'`
    fn_assert_variable_is_set "partition_id" $partition_id

    partition_column=`echo ${PARTITIONS} | awk -F '=' '{ print $1 }'`
    fn_assert_variable_is_set "partition_column" $partition_column

    fn_log_info "Loading into staging table : ${partition_id} = ${partition_column}"
    fn_log_info "Text file being loaded : ${gs_location}/$PARTITIONS"

    PART_FILES=(`gsutil ls $gs_location/$PARTITIONS/ | tail -n +2 `)
    fn_assert_variable_is_set "PART_FILES" $PART_FILES

    fn_log_info $PART_FILES
    for PART_FILE in "${PART_FILES[@]}"
    do
        QUERY=${BQ_STAGE_TABLE}'$'${partition_id}
        fn_assert_variable_is_set "QUERY" $QUERY

        fn_log_info "query=${QUERY}"
        bq load --source_format=AVRO $QUERY ${PART_FILE}
        exit_code=`echo "$?"`
        if [ $exit_code -ne 0 ]
        then
            partition_error_record=("${partition_error_record[@]}" $PART_FILE)
        fi
    done
done

  #End of loading text file partitions into staging table
   #Loading data from staging big-query to final big-query table.
   bq cp --append_table ${BQ_STAGE_TABLE} ${BQ_DATASET}
   error_code=`echo "$?"`

  #Creating Audit of the failed partitions into mysql-audit-table.
   ERROR_PARTITIONS_LENGTH=${#partition_error_record[@]}
  if [ ${ERROR_PARTITIONS_LENGTH} -ne 0 ]
  then
    fn_log_info "Started to persist the failed partition details ......"
    for ERROR_PARTITIONS in "${partition_error_record[@]}"
    do
        fn_run_java ${APPLICATION_INSTALL_DIR} "com.utility.migrator.loader_utility.PersistFailedPartitions -f ${FLOW_NAME} -t ${HIVE_TEXT_TABLE_NAME} -p ${ERROR_PARTITIONS} -c ${CONFIG_FILE_PATH}"
    done
else
    fn_log_info "No failed partitions found, nothing to persist."
fi

  #deleting backup and staging bq table
  if [ $error_code -eq 0 ]
    then
      bq rm -f ${BQ_STAGE_TABLE}
      bq rm -f ${BQ_DATASET}_backup
      bq rm -f ${BQ_DATASET}_stage_partition
  else
      fn_handle_exit_code_modules "${error_code}" "Successfully deleted stage,backup and stage_partition tables" "Failed to delete stage,backup and stage_partition tables!!!! FAILED IN BQ-LOADER-UTILITY" "${FAIL_ON_ERROR}" "${FLOW_NAME}" "${SYNC_TABLE}"
  fi

###############################################################################
#                                     End                                     #
###############################################################################
