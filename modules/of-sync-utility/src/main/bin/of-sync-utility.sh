#!/bin/bash
###############################################################################
#                               Documentation                                 #
###############################################################################
#                                                                             #
# Description                                                                 #
#     :
#                                                                             #
#                                                                             #
#                                                                             #
###############################################################################
#                           Identify Script Home                              #
###############################################################################

#Find the script file home
pushd . > /dev/null
SCRIPT_DIRECTORY="${BASH_SOURCE[0]}";
while([ -h "${SCRIPT_DIRECTORY}" ]);
do
    cd "`dirname "${SCRIPT_DIRECTORY}"`"
    SCRIPT_DIRECTORY="$(readlink "`basename "${SCRIPT_DIRECTORY}"`")";
done
cd "`dirname "${SCRIPT_DIRECTORY}"`" > /dev/null
SCRIPT_DIRECTORY="`pwd`";
popd  > /dev/null
MODULE_HOME="`dirname "${SCRIPT_DIRECTORY}"`"

fn_assert_variable_is_set "MODULE_NAME" $MODULE_HOME

###############################################################################
#                           Import Dependencies                               #
###############################################################################

#Load common dependencies
. ${MODULE_HOME}/bin/import-dependecies.sh

###############################################################################
#                                CODE                                         #
###############################################################################

fn_log_info "I am in validation step"

fn_assert_variable_is_set "APPLICATION_INSTALL_DIR" $APPLICATION_INSTALL_DIR

AIRBNB_JAR_PATH=${APPLICATION_INSTALL_DIR}/of-sync-utility/etc/airbnb-reair-main-1.0.0.jar
chmod 777 ${AIRBNB_JAR_PATH}

fn_assert_variable_is_set "AIRBNB_JAR_PATH" $AIRBNB_JAR_PATH

while getopts ":t:f:" opt; do
    case $opt in
        t) SYNC_TABLE="$OPTARG"
        ;;
        f) FLOW_NAME="$OPTARG"
        ;;
        *) fn_log_info "Invalid option -$OPTARG"
        ;;
    esac
done

fn_assert_variable_is_set "FLOW_NAME" $FLOW_NAME
fn_assert_variable_is_set "SYNC_TABLE" $SYNC_TABLE

CONFIG_FILE_PATH=${CONFIG_HOME}/${FLOW_NAME}"-"${SYNC_TABLE}"/sync-configurations.xml"
fn_assert_variable_is_set "CONFIG_FILE_PATH" $CONFIG_FILE_PATH

TEXT_FILE_PATH=${SYNC_TABLE}"_text"
fn_assert_variable_is_set "TEXT_FILE_PATH" $TEXT_FILE_PATH

#Update the status of the job
fn_run_java ${APPLICATION_INSTALL_DIR} "com.utility.migrator.sql_utility.JobTaskUpdater -f ${FLOW_NAME} -t ${SYNC_TABLE} -jst reair-sync-utility -jss RUNNING -c ${CONFIG_FILE_PATH}"

#Creating table list in HDFS
PATH_NAME=/migrator/${FLOW_NAME}'-'${SYNC_TABLE}/
fn_assert_variable_is_set "PATH_NAME" $PATH_NAME

FILE_NAME=${PATH_NAME}${SYNC_TABLE}".txt"
fn_assert_variable_is_set "FILE_NAME" $FILE_NAME

hadoop fs -rmr ${PATH_NAME}
hadoop fs -mkdir ${PATH_NAME}
fn_log_info ${SYNC_TABLE} | hadoop fs -put - ${FILE_NAME}

hadoop jar ${AIRBNB_JAR_PATH} com.airbnb.reair.batch.hive.MetastoreReplicationJob \
-D dfs.ha.namenodes.KOHLSBIGDATA2NNHA=nn1,nn2 -D dfs.namenode.rpc-address.KOHLSBIGDATA2NNHA.nn1=10.200.32.132:8020 \
-D dfs.namenode.rpc-address.KOHLSBIGDATA2NNHA.nn2=10.200.32.132:8020 -D dfs.nameservices=KOHLSBIGDATA2NNHA,ddh-dev-dataproc \
-D dfs.client.failover.proxy.provider.KOHLSBIGDATA2NNHA=org.apache.hadoop.hdfs.server.namenode.ha.ConfiguredFailoverProxyProvider \
-D dfs.ha.namenodes.ddh-dev-dataproc=nn1,nn2 -D dfs.namenode.rpc-address.ddh-dev-dataproc.nn1=ddh-dev-dataproc-m-1:8020 \
-D dfs.namenode.rpc-address.ddh-dev-dataproc.nn2=ddh-dev-dataproc-m-0:8020 \
-D dfs.client.failover.proxy.provider.ddh-dev-dataproc=org.apache.hadoop.hdfs.server.namenode.ha.ConfiguredFailoverProxyProvider \
--config-files ${CONFIG_FILE_PATH} --table-list ${FILE_NAME}

exit_code=`fn_get_exit_code $?`
fn_handle_exit_code_modules "${exit_code}" "Meta Data Sync Successfull" "Failed to sync the metadata" "${FAIL_ON_ERROR}" "${FLOW_NAME}" "${SYNC_TABLE}"

#Destination Table Location Correction and MSCK REPAIR TABLE UPDATOR
fn_run_java ${APPLICATION_INSTALL_DIR} "com.utility.migrator.sync_utility.LocationCorrector -t ${SYNC_TABLE} -c ${CONFIG_FILE_PATH}"

REGEX_LOCATION_STRING=$(fn_log_info $SYNC_TABLE | sed 's/\./\//')
fn_assert_variable_is_set "REGEX_LOCATION_STRING" $REGEX_LOCATION_STRING


#Creation of AVRO Schema File in HDFS
TEXT_TBL_LOCATION=`hive -S -e "describe formatted  ${SYNC_TABLE};" | grep 'Location' | grep 'gs://' | awk '{ print $NF }' | tail -n 1`
fn_assert_variable_is_set "TEXT_TBL_LOCATION" $TEXT_TBL_LOCATION

AVRO_TBL_LOCATION=${TEXT_TBL_LOCATION}"_text_avro"
fn_assert_variable_is_set "AVRO_TBL_LOCATION" $AVRO_TBL_LOCATION

hadoop fs -rm -r ${AVRO_TBL_LOCATION}

AVRO_SCHEMA_FILE_PATH=${CONFIG_HOME}/${FLOW_NAME}"-"${SYNC_TABLE}"/avro_schema.avsc"
fn_assert_variable_is_set "AVRO_SCHEMA_FILE_PATH" $AVRO_SCHEMA_FILE_PATH

fn_run_java ${APPLICATION_INSTALL_DIR} "com.utility.migrator.sync_utility.CreateAvroSchema -t ${SYNC_TABLE} -c ${CONFIG_FILE_PATH}" -p ${AVRO_SCHEMA_FILE_PATH}
fn_assert_variable_is_set "AVRO_SCHEMA" $AVRO_SCHEMA

hadoop fs -put  ${AVRO_SCHEMA_FILE_PATH} ${AVRO_TBL_LOCATION}

#Incremental Hive Table Automator
fn_run_java ${APPLICATION_INSTALL_DIR} "com.utility.migrator.sync_utility.IncrementalHiveTableAutomator -f ${FLOW_NAME} -t ${SYNC_TABLE} -c ${CONFIG_FILE_PATH}"

#Text Table Automator
fn_run_java ${APPLICATION_INSTALL_DIR} "com.utility.migrator.sync_utility.TextTableLoader -f ${FLOW_NAME} -t ${SYNC_TABLE} -tfp ${APPLICATION_INSTALL_DIR}/of-sync-utility/etc/schema/TextTableTemplate.vm -c ${CONFIG_FILE_PATH} -afp ${AVRO_TBL_LOCATION}"

hive -f ${CONFIG_HOME}/${FLOW_NAME}"-"${SYNC_TABLE}"/hiveQuery.hql"
exit_code=`fn_get_exit_code $?`
fn_handle_exit_code_modules "${exit_code}" "Successfully completed all subsequent jobs to completely load the data in the text table" "Failed to load the data in the text table" "${FAIL_ON_ERROR}" "${FLOW_NAME}" "${SYNC_TABLE}"

#Update the status of the job
fn_run_java ${APPLICATION_INSTALL_DIR} "com.utility.migrator.sql_utility.JobTaskUpdater -f ${FLOW_NAME} -t ${SYNC_TABLE} -jst text-table-loader-utility -jss RUNNING -c ${CONFIG_FILE_PATH}"

#Renaming the Table Partitions in case of Incremental loading
fn_run_java ${APPLICATION_INSTALL_DIR} "com.utility.migrator.sync_utility.RenameTextTablePartitions -f ${FLOW_NAME} -t ${SYNC_TABLE} -c ${CONFIG_FILE_PATH}"

#Update the status of the job
fn_run_java ${APPLICATION_INSTALL_DIR} "com.utility.migrator.sql_utility.JobTaskUpdater -f ${FLOW_NAME} -t ${SYNC_TABLE} -jst text-table-rename-partitions -jss RUNNING -c ${CONFIG_FILE_PATH}"

###############################################################################
#                                     End                                     #
###############################################################################
