#!/bin/bash
###############################################################################
#                               Documentation                                 #
###############################################################################
#                                                                             #
# Description                                                                 #
#     :
#                                                                             #
#                                                                             #
#                                                                             #
###############################################################################
#                           Identify Script Home                              #
###############################################################################
#Find the script file home
pushd . > /dev/null
SCRIPT_DIRECTORY="${BASH_SOURCE[0]}";
while([ -h "${SCRIPT_DIRECTORY}" ]);
do
  cd "`dirname "${SCRIPT_DIRECTORY}"`"
  SCRIPT_DIRECTORY="$(readlink "`basename "${SCRIPT_DIRECTORY}"`")";
done
cd "`dirname "${SCRIPT_DIRECTORY}"`" > /dev/null
SCRIPT_DIRECTORY="`pwd`";
popd  > /dev/null
MODULE_HOME="`dirname "${SCRIPT_DIRECTORY}"`"

fn_assert_variable_is_set "MODULE_NAME" $MODULE_HOME

###############################################################################
#                           Import Dependencies                               #
###############################################################################

#Load common dependencies
. ${MODULE_HOME}/bin/import-dependecies.sh

###############################################################################
#                                CODE                                         #
###############################################################################

while getopts ":t:f:" opt; do
    case $opt in
        t) SYNC_TABLE="$OPTARG"
        ;;
        f) FLOW_NAME="$OPTARG"
        ;;
        *) fn_log_info "Invalid option -$OPTARG"
        ;;
    esac
done

fn_assert_variable_is_set "FLOW_NAME" $FLOW_NAME
fn_assert_variable_is_set "SYNC_TABLE" $SYNC_TABLE

SUCCESS_MESSAGE=""
FAILURE_MESSAGE=""

CONFIG_FILE_PATH=${CONFIG_HOME}/${FLOW_NAME}"-"${SYNC_TABLE}"/sync-configurations.xml"
fn_assert_variable_is_set "CONFIG_FILE_PATH" $CONFIG_FILE_PATH

HIVE_TEXT_TABLE_NAME=${SYNC_TABLE}"_text"
fn_assert_variable_is_set "HIVE_TEXT_TABLE_NAME" $HIVE_TEXT_TABLE_NAME

#S3_BUCKET_NAME=$(fn_run_java ${APPLICATION_INSTALL_DIR} "com.utility.migrator.s3_utility.GetS3BucketName -t ${SYNC_TABLE} -c ${CONFIG_FILE_PATH})"
#fn_assert_variable_is_set "S3_BUCKET_NAME" $S3_BUCKET_NAME

S3_BUCKET_NAME=`mysql -h ${HOST} --port 3307 -u ${SQL_USR} -p${SQL_PASS} -e "select s3_bucket_name from ${DB_NAME}.bq_dictionary where ddh_table_name='${SYNC_TABLE}' limit 1"`
S3_BUCKET_NAME=`echo $S3_BUCKET_NAME | cut -d ' ' -f2`

fn_log_info "S3 Bucket Name is = ${S3_BUCKET_NAME}"

S3_COPY_PATH=${S3_BUCKET_NAME}"/"${SYNC_TABLE}
fn_assert_variable_is_set "S3_COPY_PATH" $S3_COPY_PATH

declare -a partition_error_record=();

  #Code to check if the Avro based Hive Table exists
  hive -e "describe ${HIVE_TEXT_TABLE_NAME}" >> /dev/null
  err_code=`fn_log_info "$?"`

if [ $err_code -ne 0 ]
    then
    fn_handle_exit_code_modules "${err_code}" "Text Formatted Hive Table  exists " "Text Formatted Hive Table doesn't exists FAILED IN S3-LOADER-UTILITY" "${FAIL_ON_ERROR}" "${FLOW_NAME}" "${SYNC_TABLE}"
fi

GS_TABLE_LOCATION=`hive -S -e "describe formatted  ${HIVE_TEXT_TABLE_NAME};" | grep 'Location'| awk '{ print $NF }'`
fn_assert_variable_is_set "GS_TABLE_LOCATION" $GS_TABLE_LOCATION

  fn_log_info "Google Storage Location for the text table Loader file is ${GS_TABLE_LOCATION}"
  count=`hive -e "select count(*) from ${HIVE_TEXT_TABLE_NAME}"`
  partition_array=(`hive -S -e "show partitions ${HIVE_TEXT_TABLE_NAME};"`)
  ingestion_time=`date +%Y%m%d`
   if [ ${count} -ne 0 ]
   	then
   if [ ${#partition_array[@]} -eq 1 ]
   	then
       part_id=`fn_log_info ${partition_array[0]} | awk -F '=' '{ print $2 }'`
   fi
   if [ ${#partition_array[@]} -eq 1 ] && [ "$part_id" == "$ingestion_time" ]
     then
     fn_log_info "non-partitioned table "
     fn_log_info "delete all the data from ${S3_COPY_PATH}*"
     list=(`gsutil ls s3://kohls-poc-dp-ecom/adityadb1.final_phase_avro_poc/`)
     fn_log_info ${list[0]}
     gsutil rm  ${list[0]}"*"
   else
     fn_log_info "partitioned table"
     for  PARTITIONS in "${partition_array[@]}"
     do
      fn_log_info "gsutil rm ${S3_COPY_PATH}"/"${PARTITIONS}"/*""
      gsutil rm ${S3_COPY_PATH}"/"${PARTITIONS}"/*"
     done
   fi
   fi
     #Migrate from the Partition Start into Amazon S3
  # gsutil -m cp -R ${GS_TABLE_LOCATION} ${S3_COPY_PATH}

#Instead of a simple migration, we have decided to sync the complete subdirectory
gsutil -m rsync -r ${GS_TABLE_LOCATION} ${S3_COPY_PATH}

###############################################################################
#                                     End                                     #
###############################################################################
