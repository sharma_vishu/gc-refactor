#!/bin/bash
###############################################################################
#                               Documentation                                 #
###############################################################################
#                                                                             #
# Description                                                                 #
#     :
#                                                                             #
#                                                                             #
#                                                                             #
###############################################################################
#                           Identify Script Home                              #
###############################################################################

#Find the script file home
pushd . > /dev/null
SCRIPT_DIRECTORY="${BASH_SOURCE[0]}";
while([ -h "${SCRIPT_DIRECTORY}" ]);
do
    cd "`dirname "${SCRIPT_DIRECTORY}"`"
    SCRIPT_DIRECTORY="$(readlink "`basename "${SCRIPT_DIRECTORY}"`")";
done
cd "`dirname "${SCRIPT_DIRECTORY}"`" > /dev/null
SCRIPT_DIRECTORY="`pwd`";
popd  > /dev/null
MODULE_HOME="`dirname "${SCRIPT_DIRECTORY}"`"

fn_assert_variable_is_set "MODULE_NAME" $MODULE_HOME

###############################################################################
#                           Import Dependencies                               #
###############################################################################

#Load common dependencies
. ${MODULE_HOME}/bin/import-dependecies.sh

###############################################################################
#                                CODE                                         #
###############################################################################


while getopts ":t:f:" opt; do
    case $opt in
        t) SYNC_TABLE="$OPTARG"
        ;;
        f) FLOW_NAME="$OPTARG"
        ;;
        *) fn_log_info "Invalid option -$OPTARG"
        ;;
    esac
done

fn_assert_variable_is_set "FLOW_NAME" $FLOW_NAME
fn_assert_variable_is_set "SYNC_TABLE" $SYNC_TABLE

SUCCESS_MESSAGE='Successfully completed executing Flow Registration.'
FAILURE_MESSAGE='Failed to complete flow registration,FAILURE IN FLOW INITIALIZER'

VT_CONF_FILE=${APPLICATION_INSTALL_DIR}/of-flow-initializer/etc/reair-configurations.vm
fn_assert_variable_is_set "VT_CONF_FILE" $VT_CONF_FILE

CONFIG_FILE_PATH=${CONFIG_HOME}/${FLOW_NAME}"-"${SYNC_TABLE}"/sync-configurations.xml"
fn_assert_variable_is_set "CONFIG_FILE_PATH" $CONFIG_FILE_PATH

CONFIG_PROPERTIES_FILE=${APPLICATION_INSTALL_DIR}/config/reair-configurations.properties
fn_assert_variable_is_set "CONFIG_PROPERTIES_FILE" $CONFIG_PROPERTIES_FILE

fn_log_info $VT_CONF_FILE
fn_log_info $CONFIG_FILE_PATH

#Automated Configuration File Generator
fn_run_java ${APPLICATION_INSTALL_DIR} "com.utility.migrator.initializer.AutomatedConfigGenerator -cf ${CONFIG_PROPERTIES_FILE} -vm ${VT_CONF_FILE} -t ${SYNC_TABLE} -c ${CONFIG_HOME} -f ${FLOW_NAME}"

#Validating if this particular flow isn't already runnning, if not - registering this as a new flow
fn_run_java ${APPLICATION_INSTALL_DIR} "com.utility.migrator.initializer.JobInitializater -f ${FLOW_NAME} -t ${SYNC_TABLE} -c ${CONFIG_FILE_PATH}"

###############################################################################
#                                     End                                     #
###############################################################################
