package com.utility.migrator.iNotify;

import java.io.IOException;
import java.net.URI;
import java.util.Arrays;

import com.utility.migrator.initializer.SQLUtility;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hdfs.DFSInotifyEventInputStream;
import org.apache.hadoop.hdfs.client.HdfsAdmin;
import org.apache.hadoop.hdfs.inotify.Event;
import org.apache.hadoop.hdfs.inotify.Event.CreateEvent;
import org.apache.hadoop.hdfs.inotify.Event.UnlinkEvent;
import org.apache.hadoop.hdfs.inotify.EventBatch;
import org.apache.hadoop.hdfs.inotify.MissingEventsException;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

public class Notify {
    public static Logger LOG = Logger.getLogger(Notify.class);
    public static void main(String[] args) {
        BasicConfigurator.configure();
        long lastReadTxid = 0;
        if (args.length > 1) {
            lastReadTxid = Long.parseLong(args[1]);
        }

        LOG.info("lastReadTxid = " + lastReadTxid);
        try {
            HdfsAdmin admin = new HdfsAdmin(URI.create(args[0]), new Configuration());

            DFSInotifyEventInputStream eventStream = admin.getInotifyEventStream(lastReadTxid);

            while (true) {
                EventBatch batch = eventStream.take();
                LOG.info("TxId = " + batch.getTxid());

                for (Event event : batch.getEvents()) {
                    LOG.info("event type = " + event.getEventType());
                    switch (event.getEventType()) {
                        case CREATE:
                            CreateEvent createEvent = (CreateEvent) event;
                            LOG.info("Create Event has occurred .....");
                            LOG.info("  path = " + createEvent.getPath());
                            LOG.info("  owner = " + createEvent.getOwnerName());
                            LOG.info("  ctime = " + createEvent.getCtime());
                            break;
                        case UNLINK:
                            UnlinkEvent unlinkEvent = (UnlinkEvent) event;
                            LOG.info("Delete Event has occurred");
                            LOG.info("  path = " + unlinkEvent.getPath());
                            LOG.info("  timestamp = " + unlinkEvent.getTimestamp());
                            break;

                        case APPEND:
                            Event.AppendEvent appendEvent = (Event.AppendEvent) event;
                            LOG.info("Append event has occurred ....");
                            LOG.info(" path = " + appendEvent.getPath());
                        case CLOSE:
                        case RENAME:
                            Event.RenameEvent renameEvent = (Event.RenameEvent) event;
                            LOG.info("Rename Event has occurred ....");
                            LOG.info(" source path = " + renameEvent.getSrcPath());
                            LOG.info(" destination path = " + renameEvent.getDstPath());
                            LOG.info(" timestamp = " + renameEvent.getTimestamp());
                        default:
                            break;
                    }
                }
            }
        } catch (InterruptedException e) {
            LOG.error("Notify Main : Interrupted Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (MissingEventsException e) {
            LOG.error("Notify Main : Missing event Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (IOException e) {
            LOG.error("Notify Main : IO Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
    }
}
