package com.utility.migrator.s3_utility;
;
import com.utility.migrator.initializer.SQLUtility;
import com.utility.migrator.shema_validator.SqlUtilities;
import com.utility.migrator.xml_parser.ConfigurationXMLParser;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;

public class GetS3BucketName {
    public static Logger LOG = Logger.getLogger(GetS3BucketName.class);

    public static void main(String[] args) {
            Options options = new Options();

            Option table_name = new Option("t", "sync_table_name", true, "sync table name");
            Option config_file_path = new Option("c", "config_file_path", true, "config file path");

            table_name.setRequired(true);
            config_file_path.setRequired(true);

            options.addOption(table_name);
            options.addOption(config_file_path);

            CommandLineParser parser = new DefaultParser();
            HelpFormatter formatter = new HelpFormatter();
            CommandLine cmd;

            try {
                cmd = parser.parse(options, args);
            } catch (ParseException e) {
                LOG.error("GetS3BucketName : Parse Exception met"+e.getMessage());
                formatter.printHelp("utility-name", options);

                System.exit(1);
                return;
            }

            String tableName = cmd.getOptionValue("sync_table_name");
            String confFilePath = cmd.getOptionValue("config_file_path");
            System.out.println(new GetS3BucketName().getS3BucketName(tableName, confFilePath));
            LOG.info("GetS3BucketName : "+new GetS3BucketName().getS3BucketName(tableName, confFilePath));

    }

    public String getS3BucketName (String tableName, String configFilePath) {
        String S3BucketName="";
        try {
            String mySqlDBName = new ConfigurationXMLParser().getXMLProperty("clusters.mysql.db.name", configFilePath);
            String mySqlTblName = new ConfigurationXMLParser().getXMLProperty("clusters.mysql.dictionary.table.name", configFilePath);
            String mySqlTableName = mySqlDBName.concat("." + mySqlTblName);
            Connection connection = new SqlUtilities().getSQLConnection(configFilePath);
            Statement statement = connection.createStatement();
            ResultSet s3ResultSet = statement.executeQuery("SELECT s3_bucket_name FROM " + mySqlTableName + " WHERE ddh_table_name='" + tableName + "'");

            while (s3ResultSet.next())
                S3BucketName=s3ResultSet.getString(1);

            statement.close();
            connection.close();
        } catch (SQLException sqle) {
            LOG.error("GetS3BucketName Main : SQL Exception met ....");
            LOG.error(Arrays.toString(sqle.getStackTrace()));
        } catch (Exception e) {
            LOG.error("GetS3BucketName Main : Java Lang Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
        return S3BucketName;
    }
}
