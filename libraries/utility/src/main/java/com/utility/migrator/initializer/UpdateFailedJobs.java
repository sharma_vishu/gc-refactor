package com.utility.migrator.initializer;

import com.utility.migrator.xml_parser.ConfigurationXMLParser;
import org.apache.commons.cli.*;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;

public class UpdateFailedJobs {
    public static Logger LOG = Logger.getLogger(UpdateFailedJobs.class);

    public static void main(String[] args) {
        BasicConfigurator.configure();
        Options options = new Options();
        Option flow_name = new Option("f", "flow_name", true, "flow name");
        Option table_name = new Option("t", "sync_table_name", true, "sync table name");
        Option config_file_path = new Option("c", "config_file_path", true, "config file path");

        flow_name.setRequired(true);
        table_name.setRequired(true);
        config_file_path.setRequired(true);

        options.addOption(flow_name);
        options.addOption(table_name);
        options.addOption(config_file_path);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            LOG.info("UpdatedFailedJobs : Parse Exception met "+ e.getMessage());
            formatter.printHelp("utility-name", options);

            System.exit(1);
            return;
        }

        String flowName = cmd.getOptionValue("flow_name");
        String tableName = cmd.getOptionValue("sync_table_name");
        String confFilePath = cmd.getOptionValue("config_file_path");
        try {
            String mySqlDbName = new ConfigurationXMLParser().getXMLProperty("clusters.mysql.db.name", confFilePath);
            String mySqlTblName = new ConfigurationXMLParser().getXMLProperty("clusters.mysql.flow.table.name", confFilePath);
            String mySqlTableName = mySqlDbName.concat(".").concat(mySqlTblName);

            //Get Maximum Execution Id
            String maxExecIdQuery = "SELECT max(executionId) from SQL_TABLE_NAME where table_name='FLOW_TBL_NAME' and flow_name='FLOW_NAME'";
            maxExecIdQuery = maxExecIdQuery
                    .replace("FLOW_TBL_NAME", tableName)
                    .replace("FLOW_NAME", flowName)
                    .replace("SQL_TABLE_NAME", mySqlTableName);

            Connection con= new SQLUtility().getSQLConnection(confFilePath);
            Statement stmt= con.createStatement();
            ResultSet maxExecIdResultSet = stmt.executeQuery(maxExecIdQuery);
            long maxExecutionId = -1;
            while (maxExecIdResultSet.next())
                maxExecutionId = maxExecIdResultSet.getLong(1);

            if (maxExecutionId == -1) {
                LOG.info("No previous matching execution id found ...");
                System.exit(1);
            }

            String sqlQuery = "UPDATE " + mySqlTableName + " SET status='FAILED' where executionId=" + maxExecutionId;
            LOG.info("SQL Query being ran is = " + sqlQuery);

            stmt.executeUpdate(sqlQuery);
            LOG.info("Flow Successfully Updated to failed status ! ");
            stmt.close();
            con.close();
        } catch (SQLException e){
            LOG.error("UpdateFailedJobs Main : SQL Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }catch (Exception e) {
            LOG.error("UpdateFailedJobs Main : Java lang Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
    }
}
