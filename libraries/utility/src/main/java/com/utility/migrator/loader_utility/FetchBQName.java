package com.utility.migrator.loader_utility;

import com.utility.migrator.initializer.SQLUtility;
import com.utility.migrator.xml_parser.ConfigurationXMLParser;
import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;

public class FetchBQName {
    static Logger LOG = LoggerFactory.getLogger(FetchBQName.class);
    private static ConfigurationXMLParser configurationXMLParser = new ConfigurationXMLParser();
    public static void main(String[] args) {

        Options options = new Options();

        Option ddhSchema = new Option("t", "ddh_schema_table", true, "ddh schema");
        Option confFilePath = new Option("c", "config_file_path", true, "config file path");

        ddhSchema.setRequired(true);
        confFilePath.setRequired(true);

        options.addOption(ddhSchema);
        options.addOption(confFilePath);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            LOG.error("FetchBQName : Parse Exception met"+e.getMessage());
            formatter.printHelp("utility-name", options);

            System.exit(1);
            return;
        }

        String ddh_schema = cmd.getOptionValue("ddh_schema_table");
        String conf_File_Path = cmd.getOptionValue("config_file_path");

        try {
            String mySqlDbName = configurationXMLParser.getXMLProperty("clusters.mysql.db.name", conf_File_Path);
            Connection connection = new SQLUtility().getSQLConnection(conf_File_Path);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select bq_schema_name from "+mySqlDbName+".bq_dictionary where ddh_table_name='"+ddh_schema+"' LIMIT 1");

            String dataSetName = "";
            while (resultSet.next())
                dataSetName = resultSet.getString(1);

            String tableName = ddh_schema.split("\\.")[1];
            String finalBQName = dataSetName.concat("." + tableName);
            System.out.println(finalBQName);
            LOG.info("FinalBQName = "+finalBQName);
            statement.close();
            connection.close();
        } catch (SQLException e) {
            LOG.error("FetchBQNames Main : SQL Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
    }
}
