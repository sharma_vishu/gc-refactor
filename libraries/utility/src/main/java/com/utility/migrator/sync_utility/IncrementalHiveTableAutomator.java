package com.utility.migrator.sync_utility;

import com.utility.migrator.initializer.SQLUtility;
import com.utility.migrator.shema_validator.HiveUtilities;
import com.utility.migrator.shema_validator.SqlUtilities;
import com.utility.migrator.xml_parser.ConfigurationXMLParser;
import org.apache.commons.cli.*;
import org.apache.hadoop.fs.Stat;
import org.apache.log4j.BasicConfigurator;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;
import java.util.logging.Logger;

public class IncrementalHiveTableAutomator {
    private static ConfigurationXMLParser configurationXMLParser = new ConfigurationXMLParser();
    private static String driverName = "org.apache.hive.jdbc.HiveDriver";
    static org.slf4j.Logger LOG = LoggerFactory.getLogger(IncrementalHiveTableAutomator.class);

    public static void main(String[] args)  {
        BasicConfigurator.configure();
        Options options = new Options();

        Option flow_name = new Option("f", "flow_name", true, "flow name");
        Option table_name = new Option("t", "sync_table_name", true, "sync table name");
        Option config_file_path = new Option("c", "config_file_path", true, "config file path");

        flow_name.setRequired(true);
        table_name.setRequired(true);
        config_file_path.setRequired(true);

        options.addOption(flow_name);
        options.addOption(table_name);
        options.addOption(config_file_path);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            LOG.error("IncrementalHiveTableAutomator : Parse Exception met"+e.getMessage());
            formatter.printHelp("utility-name", options);

            System.exit(1);
            return;
        }

        try {
            //Command Line Arguments
            String flowName = cmd.getOptionValue("flow_name");
            String runnerTable = cmd.getOptionValue("sync_table_name");
            String destTable = cmd.getOptionValue("sync_table_name");
            String confFilePath = cmd.getOptionValue("config_file_path");

            String destOutputPath = configurationXMLParser.getXMLProperty("airbnb.reair.clusters.batch.output.dir", confFilePath);

            String sqlDbName = configurationXMLParser.getXMLProperty("clusters.mysql.db.name", confFilePath);
            String sqlTableName = configurationXMLParser.getXMLProperty("clusters.mysql.flow.table.name", confFilePath);
            String mySqlTableName = sqlDbName.concat(".").concat(sqlTableName);

            //Validate if the table is batch or incremental.
            String checkFlowExistsString = "select count(*) from " + mySqlTableName + " where flow_name='" + flowName +
                    "' and table_name='" + runnerTable + "' and status='SUCCESSFUL'";
            LOG.info("Check FLow Exists String = "+checkFlowExistsString);
            int count = new SQLUtility().getCount(checkFlowExistsString, confFilePath);
            //TODO need to change the code here ...persist partitions into mysql table.
            if (count == 0) {
                LOG.info("Batch Update : No need of Incremental Table");
                LOG.info("persisting part files into mysql metadata table");
                ArrayList<String> hiveBasedPartitions = new IncrementalHiveTableAutomator().getAllPartitionsFromHive(destTable, confFilePath);
                new IncrementalHiveTableAutomator().persistTablePartitions(destTable, flowName, confFilePath, hiveBasedPartitions);
                LOG.info("Stopping the flow here....");
            } else {

                //Create incremental Table duplicate of destination hive schema
                Map destTableDetails = new IncrementalHiveTableAutomator().getTableSchema(destTable, confFilePath);
                String destTableSchema = destTableDetails.get("Table_Schema").toString();
                String destTableLocation = destTableDetails.get("Table_Location").toString();
                LOG.info("destTableSchema=" + destTableSchema);

                //Dropping the Incremental Table as well cleaning the previous data
                String originalTableLocation = destTableLocation.substring(1, destTableLocation.length() - 1);
                new IncrementalHiveTableAutomator().HiveRunner("destination", "DROP TABLE IF EXISTS " + destTable.concat("_incremental"), confFilePath);

                String incrementalTableSchema = destTableSchema.replace(destTable, destTable.concat("_incremental")).replace("EXTERNAL", "")
                        .concat("LOCATION " + originalTableLocation.concat("_incremental").trim() + "'");
                LOG.info("Incremental Table Schema = " + incrementalTableSchema);
                //Recreating the Incremental Table
                new IncrementalHiveTableAutomator().HiveRunner("destination", incrementalTableSchema, confFilePath);

                //Comparator for the newly added partitions
                ArrayList<String> beforeUpdatePartitions = new IncrementalHiveTableAutomator().getAllPartitions(destTable, flowName, confFilePath);
                ArrayList<String> hiveBasedPartitions = new IncrementalHiveTableAutomator().getAllPartitionsFromHive(destTable, confFilePath);
                ArrayList<String> dedup_beforeUpdatePartitions = (ArrayList<String>) beforeUpdatePartitions.clone();

                new IncrementalHiveTableAutomator().persistTablePartitions(destTable, flowName, confFilePath, hiveBasedPartitions);
                ArrayList<String> postUpdatePartitions = new IncrementalHiveTableAutomator().getAllPartitions(destTable, flowName, confFilePath);
                ArrayList<String> dup_PostUpdatePartitions = (ArrayList<String>) hiveBasedPartitions.clone();

                //Find all the partfiles in new arraylist not there in the previous :
                // postUpdatePartitions.removeAll(beforeUpdatePartitions);
                dedup_beforeUpdatePartitions.removeAll(dup_PostUpdatePartitions); //B-A
                dup_PostUpdatePartitions.removeAll(beforeUpdatePartitions); //A-B
                ArrayList<String> upsertPartitions_set1 = new ArrayList<String>();
                ArrayList<String> upsertPartitions_set2 = new ArrayList<String>();
                ArrayList<String> new_partitionList = new ArrayList<String>();

                String mySQLTableName = new ConfigurationXMLParser().getXMLProperty("clusters.mysql.partitioned.table.info.name", confFilePath);
                String sqlQuery = "SELECT partitioned  from " + mySQLTableName + " WHERE table_name='" + runnerTable + "'";
                LOG.info("Executing SQL Query = " + sqlQuery);
                Connection conn=new SqlUtilities().getSQLConnection(confFilePath);
                Statement stmt=conn.createStatement();
                ResultSet partitionDetails = stmt.executeQuery(sqlQuery);
                String isPartitioned = "";
                while (partitionDetails.next())
                    isPartitioned = partitionDetails.getString(1);

                stmt.close();
                conn.close();
                if (isPartitioned.length() == 0) {
                    LOG.info("Unable to find the partition details for this particular details");
                    System.exit(07);
                }
                String loaderTemplate = "gsutil cp -R DESTINATION_PATH INCREMENTAL_PATH";
                String storageLoader = "";

                for (int i = 0; i < dup_PostUpdatePartitions.size(); i++) {
                    int firstIndex = dup_PostUpdatePartitions.get(i).indexOf('-') + 1;
                    int lastIndex = dup_PostUpdatePartitions.get(i).lastIndexOf('/');
                    String partition_list = dup_PostUpdatePartitions.get(i).substring(firstIndex, lastIndex).toString();
                    LOG.info("partition_list=" + partition_list);
                    upsertPartitions_set1.add(dup_PostUpdatePartitions.get(i).substring(firstIndex, lastIndex));
                    if (isPartitioned.equals("yes")) {
                        new_partitionList.add(partition_list.substring(partition_list.lastIndexOf('/'), partition_list.length()));
                    } else {
                        new_partitionList.add(partition_list.substring(0, partition_list.length()));
                    }
                }
                for (int i = 0; i < dedup_beforeUpdatePartitions.size(); i++) {
                    int firstIndex = dedup_beforeUpdatePartitions.get(i).indexOf('-') + 1;
                    int lastIndex = dedup_beforeUpdatePartitions.get(i).lastIndexOf('/');
                    String partition_list = dedup_beforeUpdatePartitions.get(i).substring(firstIndex, lastIndex).toString();
                    LOG.info("partition_list=" + partition_list);
                    upsertPartitions_set2.add(dedup_beforeUpdatePartitions.get(i).substring(firstIndex, lastIndex));
                    if (isPartitioned.equals("yes")) {
                        if (!new_partitionList.contains(partition_list.substring(partition_list.lastIndexOf('/'), partition_list.length()).toString()))
                            new_partitionList.add(partition_list.substring(partition_list.lastIndexOf('/'), partition_list.length()));
                    } else {
                        if (!new_partitionList.contains(partition_list.substring(0, partition_list.length()).toString()))
                            new_partitionList.add(partition_list.substring(0, partition_list.length()));
                    }
                }


                //Iterate over the newly added partitions and add it to the Incremental file
                // Iterator newPartitions = postUpdatePartitions.iterator();
                Runtime rt = Runtime.getRuntime();
                Iterator newPartitions = new_partitionList.iterator();

                //Fetching the Partition details :

                while (newPartitions.hasNext()) {
                    String destinationPath = null;
                    String partition_value = newPartitions.next().toString();
                    String incrementalTableLocation = destTableLocation.substring(0, destTableLocation.length() - 1)
                            .concat("_incremental").replace("'", "").trim();

                    LOG.info("Incremental Table Location = " + incrementalTableLocation);
                    String hiveTableLocation = destTableLocation.replace("'", "").trim();

                    LOG.info("destination_path= " + destinationPath);
                    LOG.info("partition_value =" + partition_value);

                    if (isPartitioned.equals("yes")) {
                        destinationPath = hiveTableLocation + partition_value + "/*";

                        LOG.info("Hive Table Location = " + hiveTableLocation);

                        //Adding relevant partition details
                        String incrementalTableLocationWithPartitions = incrementalTableLocation.concat(partition_value);
                        LOG.info("incrementalTableLocationWithPartitions=" + incrementalTableLocationWithPartitions);
                        //Test if the directory exists, else recreate it
                        try {

                            Process proc = rt.exec("hadoop fs -mkdir -p " + incrementalTableLocationWithPartitions);
                            proc.waitFor();

                            if (proc.exitValue() != 0)
                                System.exit(121);
                            else
                                LOG.info("Successfully Created Directory = " + incrementalTableLocationWithPartitions);
                        } catch (Exception ex) {
                            LOG.info("The GS partitioned directory already exists - " + incrementalTableLocationWithPartitions);
                        }

                        storageLoader = loaderTemplate
                                .replace("DESTINATION_PATH", destinationPath)
                                .replace("INCREMENTAL_PATH", incrementalTableLocationWithPartitions);

                        LOG.info("Loading to incremental Table for partitioned Table  " + storageLoader);
                        //Getting the Partitions in the right place ....
                        new IncrementalHiveTableAutomator().BashRunner(storageLoader);

                    } else {
                        destinationPath = partition_value + "/*";
                        storageLoader = loaderTemplate
                                .replace("DESTINATION_PATH", destinationPath)
                                .replace("INCREMENTAL_PATH", incrementalTableLocation);

                        LOG.info("Loading to incremental for non-partitioned Table " + storageLoader);
                        //Getting the Partitions in the right place ....
                        new IncrementalHiveTableAutomator().BashRunner(storageLoader);
                    }


                }
                //Registering the newly added partitions in the Hive Metastore
                new HiveUtilities().HiveRunner("destination",
                        "MSCK REPAIR TABLE " + runnerTable.concat("_incremental"), confFilePath);
            }
        } catch (SQLException e) {
            LOG.error(" IncrementalHiveTableAutomator Main : SQL Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (Exception e) {
            LOG.error("IncrementalHiveTableAutomator Main : Java lang Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
    }

    public void HiveRunner(String tableType, String query, String confFilePath) {
        BasicConfigurator.configure();
        try {
            Class.forName(driverName);

            String hiveUrl = "", hiveUser = "", hivePassword = "";

            if (tableType.equals("source")) {
                hiveUrl = configurationXMLParser.getXMLProperty("clusters.src.hive.jdbc", confFilePath);
                hiveUser = configurationXMLParser.getXMLProperty("clusters.src.hive.user", confFilePath);
                hivePassword = configurationXMLParser.getXMLProperty("clusters.src.hive.password", confFilePath);
            } else if (tableType.equals("destination")) {
                hiveUrl = configurationXMLParser.getXMLProperty("clusters.dest.hive.jdbc", confFilePath);
                hiveUser = configurationXMLParser.getXMLProperty("clusters.dest.hive.user", confFilePath);
                hivePassword = configurationXMLParser.getXMLProperty("clusters.dest.hive.password", confFilePath);
            } else {
                LOG.info("None of the configuration values caught for Hive JDBC, Hive User and Hive Password");
                System.exit(1);
            }

            Connection con = DriverManager.getConnection(hiveUrl, hiveUser, hivePassword);
            Statement stmt = con.createStatement();
            LOG.info("Executing Hive Query - " + query);
            stmt.execute(query);
            stmt.close();
            con.close();
        } catch (ClassNotFoundException e) {
            LOG.error(" IncrementalHiveTableAutomator Main : Class Not Found Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (SQLException e) {
            LOG.error(" IncrementalHiveTableAutomator Main : SQL Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (Exception e) {
            LOG.error("IncrementalHiveTableAutomator Main : Java lang Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
    }

    public void BashRunner(String command) {
        BasicConfigurator.configure();
        try {
            Runtime rt = Runtime.getRuntime();
            Process proc = rt.exec(command);
            proc.waitFor();

            if (proc.exitValue() != 0)
                System.exit(121);
            else
                LOG.info("Successfully executed Bash Command = " + command);
        }catch (InterruptedException e) {
            LOG.error(" IncrementalHiveTableAutomator Main : Interrupted Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (IOException e) {
            LOG.error(" IncrementalHiveTableAutomator Main : IO Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
    }


    public Map<String, String> getTableSchema(String hiveTblName, String confFilePath) {
        BasicConfigurator.configure();
        Map<String,String> returningValues = new HashMap<>();
        try {
            Class.forName(driverName);
            String hiveUrl, hiveUser, hivePassword;
            hiveUrl = configurationXMLParser.getXMLProperty("clusters.dest.hive.jdbc", confFilePath);
            hiveUser = configurationXMLParser.getXMLProperty("clusters.dest.hive.user", confFilePath);
            hivePassword = configurationXMLParser.getXMLProperty("clusters.dest.hive.password", confFilePath);
            Connection con = DriverManager.getConnection(hiveUrl, hiveUser, hivePassword);
            Statement stmt = con.createStatement();


            ResultSet rs = stmt.executeQuery("show create table " + hiveTblName);

            LOG.info("show create table " + hiveTblName);

            String tableDef = "", tblLocation = "";
            while (rs.next()) {
                if (rs.getString(1).equals("LOCATION")) {
                    rs.next();
                    tblLocation = rs.getString(1);
                    LOG.info("TableLocation = "+tblLocation);
                    break;
                } else
                    tableDef += rs.getString(1) + " ";
            }
            stmt.close();
            con.close();
            returningValues.put("Table_Schema", tableDef);
            returningValues.put("Table_Location", tblLocation);
        } catch (ClassNotFoundException e) {
            LOG.error(" IncrementalHiveTableAutomator Main : Class Not Found Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (SQLException e) {
            LOG.error(" IncrementalHiveTableAutomator Main : SQL Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (Exception e) {
            LOG.error("IncrementalHiveTableAutomator Main : Java lang Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
        return returningValues;
    }

    public void persistTablePartitions (String destinationTableName, String flowName, String confFilePath, ArrayList<String> tableLocation)  {
        BasicConfigurator.configure();
        try {
            LOG.info("Received Table Location size - " + tableLocation.size());
            String mySQLTblName = new ConfigurationXMLParser().getXMLProperty("clusters.mysql.partitions.metadata", confFilePath);
            String mySQLDbName = new ConfigurationXMLParser().getXMLProperty("clusters.mysql.db.name", confFilePath);

            String mySQLTableName = mySQLDbName.concat(".").concat(mySQLTblName);
            LOG.info("Persisting Table Partitions on SQL Table = " + mySQLTableName);

            Connection conn=new SqlUtilities().getSQLConnection(confFilePath);
            Statement stmt= conn.createStatement();
            stmt.execute("DELETE FROM " + mySQLTableName + " WHERE table_name='" + destinationTableName + "' AND flow_name='" + flowName + "'");

            Iterator overPartitions = tableLocation.iterator();
            String updatedTs = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
            String insertStatementTemplate = "INSERT INTO " + mySQLTableName + "(table_name, flow_name, full_partitions, last_updated) VALUES ('TABLE_NAME', 'FLOW_NAME', 'PARTITIONS', 'UPDATED_TS')";

            while (overPartitions.hasNext()) {
                String partitionItem = overPartitions.next().toString();
                String insertStatement = insertStatementTemplate
                        .replace("TABLE_NAME", destinationTableName)
                        .replace("FLOW_NAME", flowName)
                        .replace("PARTITIONS", partitionItem)
                        .replace("UPDATED_TS", updatedTs);

                LOG.info("Insert Statement - " + insertStatement);
                stmt.execute(insertStatement);
            }
            stmt.close();
            conn.close();
            LOG.info("Table Partitions successfully persisted for Table - " + destinationTableName);
        } catch (SQLException e) {
            LOG.error(" IncrementalHiveTableAutomator Main : SQL Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (Exception e) {
            LOG.error("IncrementalHiveTableAutomator Main : Java lang Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
    }

    public ArrayList<String> getAllPartitions(String destTableName, String flowName, String confFilePath)  {
        BasicConfigurator.configure();
        ArrayList<String> partitions = new ArrayList<>();
        try {
            String mySQLTableName = new ConfigurationXMLParser().getXMLProperty("clusters.mysql.partitions.metadata", confFilePath);
            Connection conn=new SqlUtilities().getSQLConnection(confFilePath);
            Statement stmt= conn.createStatement();
            ResultSet partitionedRs = stmt.executeQuery("SELECT full_partitions FROM " + mySQLTableName + " WHERE table_name='" + destTableName + "'");

            while (partitionedRs.next())
                partitions.add(partitionedRs.getString(1));
            stmt.close();
            conn.close();
        } catch (SQLException e) {
            LOG.error(" IncrementalHiveTableAutomator Main : SQL Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (Exception e) {
            LOG.error("IncrementalHiveTableAutomator Main : Java lang Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
        return partitions;
    }

    public ArrayList<String> getAllPartitionsFromHive (String hiveTableName, String confFilePath)  {
        BasicConfigurator.configure();
        ArrayList<String> partitions = new ArrayList<>();
        try {
            String tableLocation = new ConfigurationXMLParser().getXMLProperty("airbnb.reair.clusters.dest.hdfs.root", confFilePath);
            LOG.info("Table Location - " + tableLocation);

            //Fetching the Partition details :
            String mySQLTblName = new ConfigurationXMLParser().getXMLProperty("clusters.mysql.partitioned.table.info.name", confFilePath);
            String mySQLDbName = new ConfigurationXMLParser().getXMLProperty("clusters.mysql.db.name", confFilePath);

            String mySQLTableName = mySQLDbName.concat(".").concat(mySQLTblName);
            String sqlQuery = "SELECT partitioned  from " + mySQLTableName + " WHERE table_name='" + hiveTableName + "'";
            LOG.info("Executing SQL Query = " + sqlQuery);

            ResultSet partitionDetails = new SqlUtilities().getSQLConnection(confFilePath).createStatement()
                    .executeQuery(sqlQuery);

            String isPartitioned = "";
            while (partitionDetails.next())
                isPartitioned = partitionDetails.getString(1);

            if (isPartitioned.length() == 0) {
                LOG.info("Unable to find the partition details for this particular details");
                System.exit(07);
            }

            LOG.info("Is the Table Partitioned ? " + isPartitioned);

            String partitionType = tableLocation.substring(0, tableLocation.indexOf("://") + 3);
            LOG.info("Partition Type = " + partitionType);
            Runtime rt = Runtime.getRuntime();

            if (isPartitioned.equals("no")) {
                //For Non-partitioned Table
                Process proc = rt.exec("hadoop fs -du -h " + tableLocation);

                BufferedReader in = new BufferedReader(new InputStreamReader(proc.getInputStream()));
                String partition;
                while ((partition = in.readLine()) != null) {
                    if (partition.indexOf(partitionType) == -1)
                        continue;
                    String file = null;
                    String filePath = partition.substring(partition.indexOf(partitionType), partition.length());
                    LOG.info("File Path = " + filePath);
                    int size;
                    String file_size = null;
                    if (partition.split("\\s+")[1].contains("M")) {
                        size = (int) (Float.valueOf(partition.split("\\s+")[0]) * 1000000);
                        file_size = String.valueOf(size);
                        LOG.info(size + "-" + partition.split("\\s+")[2]);
                        file = size + "-" + partition.split("\\s+")[2];
                        //  partitions.add(filePath);

                    } else {
                        if (partition.split("\\s+")[1].contains("K")) {
                            size = (int) (Float.valueOf(partition.split("\\s+")[0]) * 1000);
                            file_size = String.valueOf(size);
                            LOG.info(size + "-" + partition.split("\\s+")[2]);
                            file = file_size + "-" + partition.split("\\s+")[2];

                        } else {
                            if (partition.split("\\s+")[1].contains("G")) {
                                size = (int) (Float.valueOf(partition.split("\\s+")[0]) * 1000000000);
                                file_size = String.valueOf(size);
                                LOG.info(size + "-" + partition.split("\\s+")[2]);
                                file = file_size + "-" + partition.split("\\s+")[2];

                            } else {
                                size = (int) (Float.valueOf(partition.split("\\s+")[0]) * 1);
                                file_size = String.valueOf(size);
                                LOG.info(size + "-" + partition.split("\\s+")[1]);
                                file = file_size + "-" + partition.split("\\s+")[1];

                            }
                        }

                    }
                    partitions.add(file);

                    //  partitions.add(filePath);}
                }
                in.close();
                proc.waitFor();
                LOG.info("Process Exit Value = " + proc.exitValue());
            } else {
                //For Partitioned Table
                ResultSet allParitions = new HiveUtilities().getHiveConnection("destination", confFilePath)
                        .executeQuery("show partitions " + hiveTableName);

                while (allParitions.next()) {
                    String completePartitions = tableLocation.concat("/" + allParitions.getString(1));
                    LOG.info("Complete Partitions = " + completePartitions);
                    int size;
                    String file_size = null, file = null;
                    Process proc = rt.exec("hadoop fs -du -h " + completePartitions);

                    BufferedReader in = new BufferedReader(new InputStreamReader(proc.getInputStream()));
                    String partition;
                    while ((partition = in.readLine()) != null) {
                        if (partition.indexOf(partitionType) == -1)
                            continue;

                        // LOG.info(partition);
                        String filePath = partition.substring(partition.indexOf(partitionType), partition.length());
                        if (partition.split("\\s+")[1].contains("M")) {
                            size = (int) (Float.valueOf(partition.split("\\s+")[0]) * 1000000);
                            file_size = String.valueOf(size);
                            LOG.info(size + "-" + partition.split("\\s+")[2]);
                            file = file_size + "-" + partition.split("\\s+")[2];
                            //  partitions.add(filePath);

                        } else {
                            if (partition.split("\\s+")[1].contains("K")) {
                                size = (int) (Float.valueOf(partition.split("\\s+")[0]) * 1000);
                                file_size = String.valueOf(size);
                                LOG.info(size + "-" + partition.split("\\s+")[2]);
                                file = file_size + "-" + partition.split("\\s+")[2];

                            } else {
                                if (partition.split("\\s+")[1].contains("G")) {
                                    size = (int) (Float.valueOf(partition.split("\\s+")[0]) * 1000000000);
                                    file_size = String.valueOf(size);
                                    LOG.info(size + "-" + partition.split("\\s+")[2]);
                                    file = file_size + "-" + partition.split("\\s+")[2];

                                } else {
                                    size = (int) (Float.valueOf(partition.split("\\s+")[0]) * 1);
                                    file_size = String.valueOf(size);
                                    LOG.info(size + "-" + partition.split("\\s+")[1]);
                                    file = file_size + "-" + partition.split("\\s+")[1];

                                }
                            }

                        }
                        partitions.add(file);
                    }

                    in.close();
                    proc.waitFor();
                    LOG.info("Process Exit Value = " + proc.exitValue());
                }
            }
        } catch (InterruptedException e) {
            LOG.error(" IncrementalHiveTableAutomator Main : Interrupted Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (IOException e) {
            LOG.error(" IncrementalHiveTableAutomator Main : IO Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (SQLException e) {
            LOG.error(" IncrementalHiveTableAutomator Main : SQL Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (Exception e) {
            LOG.error("IncrementalHiveTableAutomator Main : Java lang Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
        return partitions;
    }
}
