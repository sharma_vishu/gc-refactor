package com.utility.migrator.initializer;

import com.trigger.utilities.SQLUtilities;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Properties;

public class DictionaryForAutomatedConfigGenerator {

    public final Logger LOG = Logger.getLogger(DictionaryForAutomatedConfigGenerator.class);
    public String getGsBasePath (String ddhTableName, String propertyFilePath) {
        BasicConfigurator.configure();
        try {
            Statement stmt = new DictionaryForAutomatedConfigGenerator().getSQLConnection(propertyFilePath);
            ResultSet gsResultSet = stmt.executeQuery("select gs_base_path from " + new DictionaryForAutomatedConfigGenerator()
                            .getPropertyValue("TVAR_SQL_FLOW_DICTIONARY_NAME", propertyFilePath)
                            + " WHERE ddh_table_name='" + ddhTableName + "'");

            while (gsResultSet.next())
                return gsResultSet.getString(1);
            stmt.close();
        } catch (SQLException e) {
            LOG.error(" DictionaryForAutomatedConfigGenerator Main : Java SQL Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
        return null;
    }

    public String getBQSchemaName (String ddhTableName, String propertyFilePath)  {
        BasicConfigurator.configure();
        try {
            Statement stmt = new DictionaryForAutomatedConfigGenerator().getSQLConnection(propertyFilePath);
            ResultSet bqResultSet = stmt.executeQuery("select bq_schema_name from " + new DictionaryForAutomatedConfigGenerator()
                            .getPropertyValue("TVAR_SQL_FLOW_DICTIONARY_NAME", propertyFilePath)
                            + " WHERE ddh_table_name='" + ddhTableName + "'");

            while (bqResultSet.next())
                return bqResultSet.getString(1);
            stmt.close();
        }  catch (SQLException e) {
            LOG.error(" DictionaryForAutomatedConfigGenerator Main : Java SQL Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
        return null;
    }

    public String getS3BucketName (String ddhTableName, String propertyFilePath) {
        BasicConfigurator.configure();
        try {
            Statement stmt = new DictionaryForAutomatedConfigGenerator().getSQLConnection(propertyFilePath);
            ResultSet s3ResultSet = stmt.executeQuery("select s3_bucket_name from " + new DictionaryForAutomatedConfigGenerator()
                            .getPropertyValue("TVAR_SQL_FLOW_DICTIONARY_NAME", propertyFilePath)
                            + " WHERE ddh_table_name='" + ddhTableName + "'");

            while (s3ResultSet.next())
                return s3ResultSet.getString(1);
            stmt.close();
        }catch (SQLException e) {
            LOG.error(" DictionaryForAutomatedConfigGenerator Main : Java SQL Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
        return null;
    }

    public String getPropertyValue (String propertyName, String propertyFilePath)  {
        Properties configurations = new Properties();
        BasicConfigurator.configure();
        try {
            configurations.load(new FileInputStream(new File(propertyFilePath)));
        } catch (FileNotFoundException fnfe) {
            LOG.error("DictionaryForAutomatedConfigGenerator Main : No valid configuration file path found ....");
            LOG.error(Arrays.toString(fnfe.getStackTrace()));
        } catch (IOException ioe) {
            LOG.error("DictionaryForAutomatedConfigGenerator Main : IO Exception met ....");
            LOG.error(Arrays.toString(ioe.getStackTrace()));
        }
        return configurations.getProperty(propertyName);
    }

    public Statement getSQLConnection (String propertyFilePath) {
        BasicConfigurator.configure();
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            String jdbcUrl = new DictionaryForAutomatedConfigGenerator().getPropertyValue("TVAR_SQL_JDBC", propertyFilePath);
            String sqlUser = new DictionaryForAutomatedConfigGenerator().getPropertyValue("TVAR_SQL_USER", propertyFilePath);
            String sqlPassword = new DictionaryForAutomatedConfigGenerator().getPropertyValue("TVAR_SQL_PASSWORD", propertyFilePath);
            Connection connection = DriverManager.getConnection(jdbcUrl, sqlUser, sqlPassword);
            return connection.createStatement();
        } catch (SQLException sqle) {
            LOG.error("DictionaryForAutomatedConfigGenerator Main : SQL Exception met ....");
            LOG.error(Arrays.toString(sqle.getStackTrace()));
            return null;
        } catch (ClassNotFoundException e) {
            LOG.error("DictionaryForAutomatedConfigGenerator Main : CLass not found Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
            return null;
        } catch (IllegalAccessException e) {
            LOG.error("DictionaryForAutomatedConfigGenerator Main : Illegal Access Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
            return null;
        } catch (InstantiationException e) {
            LOG.error("DictionaryForAutomatedConfigGenerator Main : Instatiation Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
            return null;
        }
    }
}
