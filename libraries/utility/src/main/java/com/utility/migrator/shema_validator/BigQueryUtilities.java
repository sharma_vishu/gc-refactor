package com.utility.migrator.shema_validator;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.api.services.bigquery.Bigquery;
import com.google.api.services.bigquery.BigqueryScopes;
import com.google.api.services.bigquery.model.*;
import com.google.cloud.bigquery.*;
import com.google.cloud.bigquery.TimePartitioning;
import com.utility.migrator.xml_parser.ConfigurationXMLParser;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import static com.utility.migrator.shema_validator.SchemaValidator.bigQueryChangedKey;
import static com.utility.migrator.shema_validator.SchemaValidator.bigQueryColumns;

public class BigQueryUtilities {
    private static String driverName = "org.apache.hive.jdbc.HiveDriver";
    private ConfigurationXMLParser configurationXMLParser = new ConfigurationXMLParser();
    public final Logger LOG = Logger.getLogger(BigQueryUtilities.class);

    public void updateBigQueryTable (String bigQuerytableId, String confFilePath) {
        BasicConfigurator.configure();
        try {
            String bigQueryProjectId = configurationXMLParser.getXMLProperty("clusters.bigquery.project.id", confFilePath);
            String bigQueryDatasetId = configurationXMLParser.getXMLProperty("clusters.bigquery.schema.name", confFilePath);

            BigQuery bigquery = BigQueryOptions.getDefaultInstance().getService();


            TableId tableId = TableId.of(bigQueryDatasetId, bigQuerytableId);
            LOG.info("TableID = "+tableId);

            Schema tableSchema = bigquery.getTable(tableId).getDefinition().getSchema();

            LOG.info("TableSchema"+tableSchema);
            List<Field> fields = tableSchema.getFields();
            ArrayList<TableFieldSchema> fieldSchema = new ArrayList<>();


        /*  Fetching Present Schema of the BigQuery Table    */
            for (int i = 0; i < fields.size(); i++) {

                if (fields.get(i).getType().getValue().toString() == "RECORD") {

                    List<Field> tableFieldSchemas = fields.get(i).getFields();

                    int itr = 0;
                    List<TableFieldSchema> recordSchema = new ArrayList<>();

                    while (itr < tableFieldSchemas.size()) {

                        if (tableFieldSchemas.get(itr).getMode() == Field.Mode.NULLABLE)
                            recordSchema.add(new TableFieldSchema().setName(tableFieldSchemas.get(itr).getName())
                                    .setType(tableFieldSchemas.get(itr).getType().getValue().toString())
                                    .setMode(getMode(tableFieldSchemas.get(itr).getMode())));
                        else
                            recordSchema.add(new TableFieldSchema().setName(tableFieldSchemas.get(itr).getName())
                                    .setType(tableFieldSchemas.get(itr).getType().getValue().toString())
                                    .setMode(getMode(tableFieldSchemas.get(itr).getMode())));
                        ++itr;
                    }

                    fieldSchema.add(
                            new TableFieldSchema().setName(fields.get(i).getName()).
                                    setType(fields.get(i).getType().getValue().toString()).setMode(getMode(fields.get(i).getMode())).setFields(recordSchema));

                } else {
                    fieldSchema.add(new TableFieldSchema().setName(fields.get(i).getName()).
                            setType(fields.get(i).getType().getValue().toString()).setMode(getMode(fields.get(i).getMode())));
                    String consoleMsg = fields.get(i).getName() + "\t" + fields.get(i).getType().getValue();
                    LOG.info("ConsoleMSG = "+consoleMsg);
                }
            }

            Iterator iterator = bigQueryChangedKey.iterator();

            while (iterator.hasNext()) {
                String colName = iterator.next().toString();
                String colDataType = bigQueryColumns.get(colName);
                LOG.info(colName + "\t" + colDataType);

                if (colDataType.contains("decimal") && !colDataType.contains("struct"))
                    colDataType = "decimal";
                if ((colDataType.contains("char") || colDataType.contains("varchar")) && !colDataType.contains("struct"))
                    colDataType = "string";

                if (colDataType.contains("array<struct<")) {
                    String colDetails = colDataType.substring(colDataType.indexOf("array<struct<") + 13, colDataType.indexOf(">>"));
                    //customSplitter is used for datatypes which contain decimal(29,0) and varchar to split them properly
                    String nestedColumns[] = new BigQueryUtilities().customSplitter(colDetails).split(",");

                    List<TableFieldSchema> recordSchema = new ArrayList<>();

                    for (int itr = 0; itr < nestedColumns.length; itr++) {
                        String individualDetails[] = nestedColumns[itr].split(":");
                        String nestedColName = individualDetails[0];
                        String nestedColDataType = individualDetails[1];

                        recordSchema.add(new TableFieldSchema().setName(nestedColName).setType(new BQHiveDictionary().columnDictionary.get(nestedColDataType)).setMode("NULLABLE"));
                    }

                    fieldSchema.add(new TableFieldSchema().setName(colName).setType("RECORD").setFields(recordSchema)
                            .setMode("NULLABLE"));
                } else if (colDataType.contains("array")) {
                    String dataType = colDataType.substring(colDataType.indexOf("<") + 1, colDataType.indexOf(">"));

                    if (colDataType.contains("decimal"))
                        dataType = "decimal";
                    if (colDataType.contains("char") || colDataType.contains("varchar"))
                        dataType = "string";

                    fieldSchema.add(new TableFieldSchema().setName(colName).setType(new BQHiveDictionary().columnDictionary.get(dataType)).setMode("REPEATED"));
                } else if (colDataType.contains("struct")) {
                    String colDetails = colDataType.substring(colDataType.indexOf("<") + 1, colDataType.indexOf(">"));
                    String nestedColumns[] = new BigQueryUtilities().customSplitter(colDetails).split(",");

                    List<TableFieldSchema> recordSchema = new ArrayList<>();

                    for (int itr = 0; itr < nestedColumns.length; itr++) {
                        String individualDetails[] = nestedColumns[itr].split(":");
                        String nestedColName = individualDetails[0];
                        String nestedColDataType = individualDetails[1];
                        LOG.info(nestedColName + nestedColDataType + "\t" + new BQHiveDictionary().columnDictionary.get(nestedColDataType));

                        recordSchema.add(new TableFieldSchema().setName(nestedColName).setType(new BQHiveDictionary().columnDictionary.get(nestedColDataType)).setMode("NULLABLE"));
                    }

                    fieldSchema.add(new TableFieldSchema().setName(colName).setType("RECORD").setFields(recordSchema)
                            .setMode("NULLABLE"));
                } else if (colDataType.contains("map")) {

                    String dataTypes = colDataType.substring(colDataType.indexOf("<") + 1, colDataType.indexOf(">"));
                    LOG.info("DataTypes - " + dataTypes);
                    String splitDataTypes[] = dataTypes.split(",");

                    List<TableFieldSchema> recordSchema = new ArrayList<>();

                    recordSchema.add(new TableFieldSchema().setName("key").setType(new BQHiveDictionary().columnDictionary.get(splitDataTypes[0]))
                            .setMode("NULLABLE"));

                    recordSchema.add(new TableFieldSchema().setName("value").setType(new BQHiveDictionary().columnDictionary.get(splitDataTypes[1]))
                            .setMode("NULLABLE"));

                    fieldSchema.add(new TableFieldSchema().setName(colName).setType("RECORD").setFields(recordSchema)
                            .setMode("REPEATED"));

                    LOG.info("FieldSchema"+fieldSchema);
                    LOG.info("End of Map");
                } else {
                    LOG.info(colDataType + "\t" + new BQHiveDictionary().columnDictionary.get(colDataType));
                    fieldSchema.add(new TableFieldSchema().setName(colName).setType(new BQHiveDictionary().columnDictionary.get(colDataType)).setMode("NULLABLE"));
                }
            }

            TableSchema schema = new TableSchema();
            schema.setFields(fieldSchema);

            TableReference ref = new TableReference();
            ref.setProjectId(bigQueryProjectId);
            ref.setDatasetId(bigQueryDatasetId);
            ref.setTableId(bigQuerytableId);

            com.google.api.services.bigquery.model.Table content = new com.google.api.services.bigquery.model.Table();
            content.setTableReference(ref);
            content.setSchema(schema);

            com.google.api.services.bigquery.model.TimePartitioning timePartitioning = new com.google.api.services.bigquery.model.TimePartitioning();
            timePartitioning.setType("DAY");
            content.setTimePartitioning(timePartitioning);

            LOG.info("FieldSchema"+fieldSchema);

            Bigquery client = createAuthorizedClient();
            client.tables().update(ref.getProjectId(), ref.getDatasetId(), bigQuerytableId, content).execute();

            LOG.info("Big Query Schema updated successfully ....");
        } catch (IOException e) {
            LOG.error("BigQueryUtilities Main : IO Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (Exception e) {
            LOG.error("BigQueryUtilities Main : Java lang Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
    }

    public void automatedBQSchemaGenerator(String hiveTableName, String confFilePath)  {
        BasicConfigurator.configure();
        try {
            String bigQueryProjectId = configurationXMLParser.getXMLProperty("clusters.bigquery.project.id", confFilePath);
            String bigQueryDatasetId = configurationXMLParser.getXMLProperty("clusters.bigquery.schema.name", confFilePath);

            Bigquery client = createAuthorizedClient();
            ArrayList<TableFieldSchema> fieldSchema = new ArrayList<TableFieldSchema>();

            String query = "describe " + hiveTableName;
            LOG.info("Executing Query = " + query);

            Class.forName(driverName);
            String hiveUrl = "", hiveUser = "", hivePassword = "";

            hiveUrl = configurationXMLParser.getXMLProperty("clusters.src.hive.jdbc", confFilePath);
            hiveUser = configurationXMLParser.getXMLProperty("clusters.src.hive.user", confFilePath);
            hivePassword = configurationXMLParser.getXMLProperty("clusters.src.hive.password", confFilePath);

            Connection con = DriverManager.getConnection(hiveUrl, hiveUser, hivePassword);
            Statement stmt = con.createStatement();
            ResultSet rsHive = stmt.executeQuery(query);

            while (rsHive.next()) {
                String colName = rsHive.getString(1);
                String colDataType = rsHive.getString(2);

                if (colName == null || colDataType == null)
                    break;

                if (colDataType.contains("decimal") && !colDataType.contains("struct"))
                    colDataType = "decimal";
                if ((colDataType.contains("char") || colDataType.contains("varchar")) && !colDataType.contains("struct"))
                    colDataType = "string";

                if (colDataType.contains("array<struct<")) {
                    String colDetails = colDataType.substring(colDataType.indexOf("array<struct<") + 13, colDataType.indexOf(">>"));
                    String nestedColumns[] = new BigQueryUtilities().customSplitter(colDetails).split(",");

                    List<TableFieldSchema> recordSchema = new ArrayList<>();

                    for (int itr = 0; itr < nestedColumns.length; itr++) {
                        String individualDetails[] = nestedColumns[itr].split(":");
                        String nestedColName = individualDetails[0];
                        String nestedColDataType = individualDetails[1];
                        recordSchema.add(new TableFieldSchema().setName(nestedColName).setType(new BQHiveDictionary().columnDictionary.get(nestedColDataType)).setMode("NULLABLE"));
                    }

                    fieldSchema.add(new TableFieldSchema().setName(colName).setType("RECORD").setFields(recordSchema)
                            .setMode("NULLABLE"));
                } else if (colDataType.contains("array")) {
                    String dataType = colDataType.substring(colDataType.indexOf("<") + 1, colDataType.indexOf(">"));

                    if (colDataType.contains("decimal"))
                        dataType = "decimal";
                    if (colDataType.contains("char") || colDataType.contains("varchar"))
                        dataType = "string";

                    fieldSchema.add(new TableFieldSchema().setName(colName).setType(new BQHiveDictionary().columnDictionary.get(dataType)).setMode("REPEATED"));
                } else if (colDataType.contains("struct")) {
                    String colDetails = colDataType.substring(colDataType.indexOf("<") + 1, colDataType.indexOf(">"));
                    String nestedColumns[] = new BigQueryUtilities().customSplitter(colDetails).split(",");

                    List<TableFieldSchema> recordSchema = new ArrayList<>();

                    for (int itr = 0; itr < nestedColumns.length; itr++) {
                        String individualDetails[] = nestedColumns[itr].split(":");
                        String nestedColName = individualDetails[0];
                        String nestedColDataType = individualDetails[1];
                        LOG.info(nestedColName + nestedColDataType + "\t" + new BQHiveDictionary().columnDictionary.get(nestedColDataType));

                        recordSchema.add(new TableFieldSchema().setName(nestedColName).setType(new BQHiveDictionary().columnDictionary.get(nestedColDataType)).setMode("NULLABLE"));
                    }

                    fieldSchema.add(new TableFieldSchema().setName(colName).setType("RECORD").setFields(recordSchema)
                            .setMode("NULLABLE"));
                } else if (colDataType.contains("map")) {

                    String dataTypes = colDataType.substring(colDataType.indexOf("<") + 1, colDataType.indexOf(">"));
                    LOG.info("DataTypes - " + dataTypes);
                    String splitDataTypes[] = dataTypes.split(",");

                    List<TableFieldSchema> recordSchema = new ArrayList<>();

                    recordSchema.add(new TableFieldSchema().setName("key").setType(new BQHiveDictionary().columnDictionary.get(splitDataTypes[0]))
                            .setMode("NULLABLE"));

                    recordSchema.add(new TableFieldSchema().setName("value").setType(new BQHiveDictionary().columnDictionary.get(splitDataTypes[1]))
                            .setMode("NULLABLE"));

                    fieldSchema.add(new TableFieldSchema().setName(colName).setType("RECORD").setFields(recordSchema)
                            .setMode("REPEATED"));

                    LOG.info("FiledSchema = "+fieldSchema);
                    LOG.info("End of Map");
                } else {
                    LOG.info(colDataType + "\t" + new BQHiveDictionary().columnDictionary.get(colDataType));
                    fieldSchema.add(new TableFieldSchema().setName(colName).setType(new BQHiveDictionary().columnDictionary.get(colDataType)).setMode("NULLABLE"));
                }
            }

            TableSchema schema = new TableSchema();
            schema.setFields(fieldSchema);

            LOG.info("FieldSchema = "+fieldSchema);

            TableReference ref = new TableReference();
            ref.setProjectId(bigQueryProjectId);
            ref.setDatasetId(bigQueryDatasetId);
            String bqTableName = hiveTableName.substring(hiveTableName.indexOf(".") + 1, hiveTableName.length());
            ref.setTableId(bqTableName);

            com.google.api.services.bigquery.model.Table content = new com.google.api.services.bigquery.model.Table();
            content.setTableReference(ref);
            content.setSchema(schema);
            com.google.api.services.bigquery.model.TimePartitioning timePartitioning = new com.google.api.services.bigquery.model.TimePartitioning();
            timePartitioning.setType("DAY");
            content.setTimePartitioning(timePartitioning);

            //Validate if the bigquery table already exists.
            client.tables().delete(ref.getProjectId(), ref.getDatasetId(), ref.getTableId()).execute();
            LOG.info("Previous Instance of the table deleted successful.");
            client.tables().insert(ref.getProjectId(), ref.getDatasetId(), content).execute();
            stmt.close();
            con.close();
        } catch (ClassNotFoundException e) {
            LOG.error("BigQueryUtilities Main : Class Not Found Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (SQLException e) {
            LOG.error("BigQueryUtilities Main : SQL Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (IOException e) {
            LOG.error("BigQueryUtilities Main : IO Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (Exception e) {
            LOG.error("BigQueryUtilities Main : Java lang Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }

    }

    public String getBigQuerySchema(String bigQueryTableName, String confFilePath) {
        BasicConfigurator.configure();
        String line = "", schemaBQ = "";
        try {
            String projectId = configurationXMLParser.getXMLProperty("clusters.bigquery.project.id", confFilePath);
            String localTableName = projectId + ":" + bigQueryTableName;

            String executionCode = "bq show --format=prettyjson " + localTableName;
            Process process = Runtime.getRuntime().exec(executionCode);
            process.waitFor();

            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

            while ((line = reader.readLine()) != null) {
                schemaBQ += line + "\n";
            }
        } catch (InterruptedException e) {
            LOG.error("BigQueryUtilities Main : Interrupted Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (IOException e) {
            LOG.error("BigQueryUtilities Main : IO Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (Exception e) {
            LOG.error("BigQueryUtilities Main : Java lang Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
        return schemaBQ;
    }

    public String getMode (Field.Mode mode) {
        BasicConfigurator.configure();
        if (mode == Field.Mode.NULLABLE)
            return  "NULLABLE";
        else if (mode == Field.Mode.REPEATED)
            return  "REPEATED";
        else if (mode == Field.Mode.REQUIRED)
            return  "REQUIRED";
        else
            return  "NULLABLE";

    }

    public Bigquery createAuthorizedClient() {
        BasicConfigurator.configure();
        // Create the credential
        HttpTransport transport = new NetHttpTransport();
        JsonFactory jsonFactory = new JacksonFactory();
        try {
            GoogleCredential credential = GoogleCredential.getApplicationDefault(transport, jsonFactory);

            if (credential.createScopedRequired()) {
                credential = credential.createScoped(BigqueryScopes.all());
            }

            return new Bigquery.Builder(transport, jsonFactory, credential)
                    .setApplicationName("BigQuery Schema Updater")
                    .build();
        } catch (IOException e) {
            LOG.error("BigQueryUtilities Main : IO Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    public void automatedBQSchemaGeneratorForStaging(String hiveTableName, String confFilePath) {
        BasicConfigurator.configure();
        try {
            String bigQueryProjectId = configurationXMLParser.getXMLProperty("clusters.bigquery.project.id", confFilePath);
            String bigQueryDatasetId = configurationXMLParser.getXMLProperty("clusters.bigquery.schema.name", confFilePath);

            Bigquery client = createAuthorizedClient();
            ArrayList<TableFieldSchema> fieldSchema = new ArrayList<TableFieldSchema>();

            String query = "describe " + hiveTableName;
            LOG.info("Executing Query = " + query);

            Class.forName(driverName);

            String hiveUrl, hiveUser, hivePassword;

            hiveUrl = configurationXMLParser.getXMLProperty("clusters.src.hive.jdbc", confFilePath);
            hiveUser = configurationXMLParser.getXMLProperty("clusters.src.hive.user", confFilePath);
            hivePassword = configurationXMLParser.getXMLProperty("clusters.src.hive.password", confFilePath);

            Connection con = DriverManager.getConnection(hiveUrl, hiveUser, hivePassword);
            Statement stmt = con.createStatement();
            ResultSet rsHive = stmt.executeQuery(query);

            while (rsHive.next()) {
                String colName = rsHive.getString(1);
                String colDataType = rsHive.getString(2);

                if (colName == null || colDataType == null)
                    break;

                if (colDataType.contains("decimal") && !colDataType.contains("struct"))
                    colDataType = "decimal";
                if ((colDataType.contains("char") || colDataType.contains("varchar")) && !colDataType.contains("struct"))
                    colDataType = "string";

                if (colDataType.contains("array<struct<")) {
                    String colDetails = colDataType.substring(colDataType.indexOf("array<struct<") + 13, colDataType.indexOf(">>"));
                    String nestedColumns[] = new BigQueryUtilities().customSplitter(colDetails).split(",");

                    List<TableFieldSchema> recordSchema = new ArrayList<>();

                    for (int itr = 0; itr < nestedColumns.length; itr++) {
                        String individualDetails[] = nestedColumns[itr].split(":");
                        String nestedColName = individualDetails[0];
                        String nestedColDataType = individualDetails[1];

                        recordSchema.add(new TableFieldSchema().setName(nestedColName).setType(new BQHiveDictionary().columnDictionary.get(nestedColDataType)).setMode("NULLABLE"));
                    }

                    fieldSchema.add(new TableFieldSchema().setName(colName).setType("RECORD").setFields(recordSchema)
                            .setMode("NULLABLE"));
                } else if (colDataType.contains("array")) {
                    String dataType = colDataType.substring(colDataType.indexOf("<") + 1, colDataType.indexOf(">"));

                    if (colDataType.contains("decimal"))
                        dataType = "decimal";
                    if (colDataType.contains("char") || colDataType.contains("varchar"))
                        dataType = "string";

                    fieldSchema.add(new TableFieldSchema().setName(colName).setType(new BQHiveDictionary().columnDictionary.get(dataType)).setMode("REPEATED"));
                } else if (colDataType.contains("struct")) {
                    String colDetails = colDataType.substring(colDataType.indexOf("<") + 1, colDataType.indexOf(">"));
                    String nestedColumns[] = new BigQueryUtilities().customSplitter(colDetails).split(",");

                    List<TableFieldSchema> recordSchema = new ArrayList<>();

                    for (int itr = 0; itr < nestedColumns.length; itr++) {
                        String individualDetails[] = nestedColumns[itr].split(":");
                        String nestedColName = individualDetails[0];
                        String nestedColDataType = individualDetails[1];
                        LOG.info(nestedColName + nestedColDataType + "\t" + new BQHiveDictionary().columnDictionary.get(nestedColDataType));

                        recordSchema.add(new TableFieldSchema().setName(nestedColName).setType(new BQHiveDictionary().columnDictionary.get(nestedColDataType)).setMode("NULLABLE"));
                    }

                    fieldSchema.add(new TableFieldSchema().setName(colName).setType("RECORD").setFields(recordSchema)
                            .setMode("NULLABLE"));
                } else if (colDataType.contains("map")) {

                    String dataTypes = colDataType.substring(colDataType.indexOf("<") + 1, colDataType.indexOf(">"));
                    LOG.info("DataTypes - " + dataTypes);
                    String splitDataTypes[] = dataTypes.split(",");

                    List<TableFieldSchema> recordSchema = new ArrayList<>();

                    recordSchema.add(new TableFieldSchema().setName("key").setType(new BQHiveDictionary().columnDictionary.get(splitDataTypes[0]))
                            .setMode("NULLABLE"));

                    recordSchema.add(new TableFieldSchema().setName("value").setType(new BQHiveDictionary().columnDictionary.get(splitDataTypes[1]))
                            .setMode("NULLABLE"));

                    fieldSchema.add(new TableFieldSchema().setName(colName).setType("RECORD").setFields(recordSchema)
                            .setMode("REPEATED"));

                    LOG.info("FieldSchema = "+fieldSchema);
                    LOG.info("End of Map");
                } else {
                    LOG.info(colDataType + "\t" + new BQHiveDictionary().columnDictionary.get(colDataType));
                    fieldSchema.add(new TableFieldSchema().setName(colName).setType(new BQHiveDictionary().columnDictionary.get(colDataType)).setMode("NULLABLE"));
                }
            }

            TableSchema schema = new TableSchema();
            schema.setFields(fieldSchema);

            LOG.info("FieldSchema = "+fieldSchema);

            TableReference ref = new TableReference();
            ref.setProjectId(bigQueryProjectId);
            ref.setDatasetId(bigQueryDatasetId);
            String bqTableName = hiveTableName.substring(hiveTableName.indexOf(".") + 1, hiveTableName.length()).concat("_stage");
            ref.setTableId(bqTableName);


            com.google.api.services.bigquery.model.Table content = new com.google.api.services.bigquery.model.Table();
            content.setTableReference(ref);
            content.setSchema(schema);
            com.google.api.services.bigquery.model.TimePartitioning timePartitioning = new com.google.api.services.bigquery.model.TimePartitioning();
            timePartitioning.setType("DAY");
            content.setTimePartitioning(timePartitioning);
            stmt.close();
            con.close();
            //Validate if the bigquery table already exists.
            try {
                client.tables().delete(ref.getProjectId(), ref.getDatasetId(), ref.getTableId()).execute();
                LOG.info("Previous Instance of the staging table deleted successful.");
            } catch (Exception ex) {
                LOG.info("Validated : No Previous instance of the staging table exists.");
            }

            client.tables().insert(ref.getProjectId(), ref.getDatasetId(), content).execute();
        } catch (ClassNotFoundException e) {
            LOG.error("BigQueryUtilities Main : Class Not Found Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (SQLException e) {
            LOG.error("BigQueryUtilities Main : SQL Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (IOException e) {
            LOG.error("BigQueryUtilities Main : IO Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (Exception e) {
            LOG.error("BigQueryUtilities Main : Java lang Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
    }

    public String customSplitter(String pattern) {
        BasicConfigurator.configure();
        String[] wordsInPattern = pattern.split(":");
        String finalString = "";
        for (int itr = 0; itr < wordsInPattern.length; ++itr) {
            if (wordsInPattern[itr].contains("decimal") && itr < wordsInPattern.length-1)
                finalString += "decimal," + wordsInPattern[itr].substring(wordsInPattern[itr].indexOf("),") + 2, wordsInPattern[itr].length()) + ":";
            else if (wordsInPattern[itr].contains("decimal") && itr == wordsInPattern.length-1)
                finalString += "decimal:";
            else if ((wordsInPattern[itr].contains("char") || wordsInPattern[itr].contains("varchar"))  && itr < wordsInPattern.length-1)
                finalString += "string," + wordsInPattern[itr].substring(wordsInPattern[itr].indexOf("),") + 2, wordsInPattern[itr].length()) + ":";
            else if ((wordsInPattern[itr].contains("char") || wordsInPattern[itr].contains("varchar")) && itr == wordsInPattern.length-1)
                finalString += "string:";
            else
                finalString += wordsInPattern[itr]+":";
        }

        finalString = finalString.substring(0, finalString.length()-1);
        return finalString;
    }

    public void BashRunner(String command) {
        BasicConfigurator.configure();
        try {
            Runtime rt = Runtime.getRuntime();
            Process proc = rt.exec(command);
            proc.waitFor();
            if (proc.exitValue() != 0)
                System.exit(121);
            else
                LOG.info("Successfully executed Bash Command = " + command);
            ;
        } catch (InterruptedException e) {
            LOG.error("BigQueryUtilities Main : Interrupted Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (IOException e) {
            LOG.error("BigQueryUtilities Main : IO Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
    }
}
