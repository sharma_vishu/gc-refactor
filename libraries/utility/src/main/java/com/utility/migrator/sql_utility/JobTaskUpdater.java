package com.utility.migrator.sql_utility;

import com.utility.migrator.xml_parser.ConfigurationXMLParser;
import org.apache.commons.cli.*;
import org.apache.log4j.BasicConfigurator;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Calendar;

public class JobTaskUpdater {
    static Logger LOG= LoggerFactory.getLogger(JobTaskUpdater.class);

    public static void main(String[] args)  {
        BasicConfigurator.configure();
        Options options = new Options();

        Option flow_name = new Option("f", "flow_name", true, "flow name");
        Option table_name = new Option("t", "sync_table_name", true, "sync table name");
        Option job_stage = new Option("jst", "job_stage", true, "job_stage");
        Option job_status = new Option("jss", "job_status", true, "job_status");
        Option config_file_path = new Option("c", "config_file_path", true, "config file path");

        flow_name.setRequired(true);
        table_name.setRequired(true);
        job_stage.setRequired(true);
        job_status.setRequired(true);
        config_file_path.setRequired(true);

        options.addOption(flow_name);
        options.addOption(table_name);
        options.addOption(job_stage);
        options.addOption(job_status);
        options.addOption(config_file_path);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            LOG.error("JobTaskUpdater : Parse Exception met"+e.getMessage());
            formatter.printHelp("utility-name", options);
            System.exit(1);
            return;
        }
        try {
            String flowName = cmd.getOptionValue("flow_name");
            String runnnerTableName = cmd.getOptionValue("sync_table_name");
            String jobStage = cmd.getOptionValue("job_stage");
            String jobStatus = cmd.getOptionValue("job_status");
            String confFilePath = cmd.getOptionValue("config_file_path");

            ConfigurationXMLParser configurationXMLParser = new ConfigurationXMLParser();

        /*String flowName  = "of-table-migration";
        String runnnerTableName = "adityadb.table1";
        String jobStage = "reair-sync-utility";
        String jobStatus = "RUNNING";
        String confFilePath = "/Users/tkmah9c/GoogleCloudMigration/GCDataMovement/bigdata-sync/config/dev/sync-configurations.xml";*/

            String sqlJdbcURL = configurationXMLParser.getXMLProperty("clusters.mysql.host", confFilePath);
            String sqlUser = configurationXMLParser.getXMLProperty("clusters.mysql.user", confFilePath);
            String sqlPassword = configurationXMLParser.getXMLProperty("clusters.mysql.password", confFilePath);
            String sqlDbName = configurationXMLParser.getXMLProperty("clusters.mysql.db.name", confFilePath);
            String sqlTableName = configurationXMLParser.getXMLProperty("clusters.mysql.flow.table.name", confFilePath);
            String mySqlTableName = sqlDbName + "." + sqlTableName;


            // Updating the present job status in MySQL Table
            String templateQuery = "UPDATE SQL_RECORD_TBL SET status='STATUS',job_stage='JOB_STAGE',loader_timestamp='TIMESTAMP' where table_name='SRC_TABLE' and flow_name='FLOW_NAME' " +
                    "and executionId = (select max(T.executionId) from (SELECT * FROM SQL_RECORD_TBL) T where T.table_name='SRC_TABLE' and T.flow_name='FLOW_NAME') ;";
            String query = templateQuery
                    .replace("SQL_RECORD_TBL", mySqlTableName)
                    .replace("STATUS", jobStatus)
                    .replace("JOB_STAGE", jobStage)
                    .replace("SRC_TABLE", runnnerTableName)
                    .replace("FLOW_NAME", flowName)
                    .replace("TIMESTAMP", DateTimeFormat.forPattern("yyyy-MM-dd-HH:mm:ss").print(new DateTime(Calendar.getInstance())));

            LOG.info("Query = "+query);
            Connection conn=getMySQLConnection(sqlJdbcURL, sqlUser, sqlPassword);
            Statement stmt=conn.createStatement();
            stmt.execute(query);

            LOG.info("Flow has been successfully registered for table - " + runnnerTableName);
            stmt.close();
            conn.close();
        } catch (SQLException e) {
            LOG.error("JobTaskUpdater Main : SQL Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (Exception e) {
            LOG.error("JobTaskUpdater Main : Java lang Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
    }

    public static Connection getMySQLConnection(String sqlJdbcURL, String sqlUser, String sqlPassword) {
        BasicConfigurator.configure();
        Connection connection=null;
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            connection = DriverManager.getConnection(sqlJdbcURL, sqlUser, sqlPassword);
        }catch (SQLException e) {
            LOG.error("SqlUtilities Main : SQL Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (Exception e) {
            LOG.error("SqlUtilities Main : Java lang Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
        return connection;
    }
}
