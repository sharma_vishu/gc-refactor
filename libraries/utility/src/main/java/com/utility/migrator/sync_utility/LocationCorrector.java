package com.utility.migrator.sync_utility;

import com.utility.migrator.shema_validator.HiveUtilities;
import com.utility.migrator.xml_parser.ConfigurationXMLParser;
import org.apache.commons.cli.*;
import org.apache.log4j.BasicConfigurator;
import org.slf4j.LoggerFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.logging.Logger;

public class LocationCorrector {
    private static String driverName = "org.apache.hive.jdbc.HiveDriver";
    static org.slf4j.Logger LOG= LoggerFactory.getLogger(LocationCorrector.class);

    public static void main(String[] args) {

        /*
            args[0] : Table Name
            args[1] : Configuration file Path
         */
        BasicConfigurator.configure();

        Options options = new Options();

        Option table_name = new Option("t", "sync_table_name", true, "sync table name");
        Option config_file_path = new Option("c", "config_file_path", true, "config file path");

        table_name.setRequired(true);
        config_file_path.setRequired(true);

        options.addOption(table_name);
        options.addOption(config_file_path);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            LOG.info("LocationCorrector : Parse Exception met"+e.getMessage());
            formatter.printHelp("utility-name", options);

            System.exit(1);
            return;
        }

        String destinationTableName = cmd.getOptionValue("sync_table_name");
        String confFilePath = cmd.getOptionValue("config_file_path");
        try {
            Class.forName(driverName);

            String orcTableExists = new LocationCorrector().checkIfTableisORC("destination", destinationTableName, confFilePath);

            if (orcTableExists.equals("false")) {

                String tableLocationCorrector = "ALTER TABLE " + destinationTableName + " SET LOCATION '" + new ConfigurationXMLParser()
                        .getXMLProperty("airbnb.reair.clusters.dest.hdfs.root", confFilePath) + "'";

                new HiveUtilities().HiveRunner("destination", tableLocationCorrector, confFilePath);

            /*
            Updating the table Partitions at the destination
            */
                new HiveUtilities().HiveRunner("destination", "MSCK REPAIR TABLE " + destinationTableName, confFilePath);
                LOG.info("Table " + destinationTableName + " partitions successfully updated ...");

            } else {


            /*
            recreating ORC main Table
             */
                String srcTableSchema = new LocationCorrector().getTableSchema("source", destinationTableName, confFilePath);
                srcTableSchema += " STORED AS ORC \n";
                srcTableSchema += " LOCATION '" + new ConfigurationXMLParser().getXMLProperty("airbnb.reair.clusters.dest.hdfs.root", confFilePath) + "'";

                String filteredTableString = "CREATE EXTERNAL".concat(
                        srcTableSchema.substring(srcTableSchema.toLowerCase().indexOf("external") + 8, srcTableSchema.length()));

                LOG.info("New Table Schema = " + filteredTableString);
                //Re-Configuring hive runner table in the destination Hive for ORC Formats
                new HiveUtilities().HiveRunner("destination", "DROP TABLE IF EXISTS " + destinationTableName, confFilePath);
                new HiveUtilities().HiveRunner("destination", filteredTableString, confFilePath);

                //Updating the table Partitions at the destination
                new HiveUtilities().HiveRunner("destination", "MSCK REPAIR TABLE " + destinationTableName, confFilePath);
                LOG.info("Table " + destinationTableName + " partitions successfully updated ...");

            }

            LOG.info("Location Corrector Stage Updated Successfully ......");
        } catch (ClassNotFoundException e) {
            LOG.error("LocationCorrector Main : Class Not Found Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (Exception e) {
            LOG.error("LocationCorrector Main : Java lang Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
    }

    public String getTableSchema(String tableType, String hiveTblName, String confFilePath)  {
        BasicConfigurator.configure();
        String tableDef="";
        try {
            Class.forName(driverName);
            Statement st = new HiveUtilities().getHiveConnection(tableType, confFilePath);
            LOG.info("show create table " + hiveTblName);
            ResultSet rs = st.executeQuery("show create table " + hiveTblName);


            while (rs.next()) {
                String item = rs.getString(1);

                if ((item.contains("ROW FORMAT SERDE") || item.contains("INPUTFORMAT") || item.contains("OUTPUTFORMAT") || item.contains("LOCATION"))
                        && !item.contains("COMMENT '"))
                    break;
                else
                    tableDef += rs.getString(1) + " ";
            }
            st.close();
        } catch (ClassNotFoundException e) {
            LOG.error("LocationCorrector Main : Class Not Found Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (SQLException e) {
            LOG.error("LocationCorrector Main : SQL Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
        return tableDef;
    }

    public String checkIfTableisORC (String tableType, String hiveTblName, String confFilePath)  {
        BasicConfigurator.configure();
        String bool="false";
        try {
            Class.forName(driverName);
            Statement st = new HiveUtilities().getHiveConnection(tableType, confFilePath);
            LOG.info("show create table " + hiveTblName);
            ResultSet rs = st.executeQuery("show create table " + hiveTblName);

            while (rs.next()) {
                String item = rs.getString(1);
                if (item.contains("OrcInputFormat") || item.contains("OrcOutputFormat"))
                    bool="true";
                else
                    continue;
            }
            st.close();
        } catch (ClassNotFoundException e) {
            LOG.error("LocationCorrector Main : Class Not Found Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (SQLException e) {
            LOG.error("LocationCorrector Main : SQL Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
        return bool;
    }
}
