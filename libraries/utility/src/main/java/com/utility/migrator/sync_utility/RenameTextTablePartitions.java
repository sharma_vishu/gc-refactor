package com.utility.migrator.sync_utility;

import com.utility.migrator.initializer.SQLUtility;
import com.utility.migrator.shema_validator.HiveUtilities;
import com.utility.migrator.xml_parser.ConfigurationXMLParser;
import org.apache.commons.cli.*;
import org.apache.log4j.BasicConfigurator;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Calendar;

public class RenameTextTablePartitions {
    static Logger LOG= LoggerFactory.getLogger(RenameTextTablePartitions.class);
    public static void main(String[] args) {

        /**
         * args[0] - Flow Name
         * args[1] - Sync Table Name
         * args[2] - Config File Path
         */
        BasicConfigurator.configure();
        Options options = new Options();

        Option flow_name = new Option("f", "flow_name", true, "flow name");
        Option table_name = new Option("t", "sync_table_name", true, "sync table name");
        Option config_file_path = new Option("c", "config_file_path", true, "config file path");

        flow_name.setRequired(true);
        table_name.setRequired(true);
        config_file_path.setRequired(true);

        options.addOption(flow_name);
        options.addOption(table_name);
        options.addOption(config_file_path);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            LOG.error("RenameTextTablePartitions : Parse Exception met "+e.getMessage());
            formatter.printHelp("utility-name", options);
            System.exit(1);
            return;
        }

        String flowName = cmd.getOptionValue("flow_name");
        String tableName = cmd.getOptionValue("sync_table_name");
        String configFilePath = cmd.getOptionValue("config_file_path");
        try {
            String sqlDbName = new ConfigurationXMLParser().getXMLProperty("clusters.mysql.db.name", configFilePath);
            String sqlTableName = new ConfigurationXMLParser().getXMLProperty("clusters.mysql.flow.table.name", configFilePath);
            String mySqlTableName = sqlDbName.concat(".").concat(sqlTableName);

            String checkFlowExistsString = "select count(*) from " + mySqlTableName + " where flow_name='" + flowName +
                    "' and table_name='" + tableName + "' and status='SUCCESSFUL'";
            LOG.info("Check Flow Exists String"+checkFlowExistsString);
            int count = new SQLUtility().getCount(checkFlowExistsString, configFilePath);

            if (count == 0) {
                LOG.info("Batch Update : No need of Partition table renaming.....");
                LOG.info("Exiting from here ....");
            } else {

                String textTableName = tableName.concat("_text");
                String textTableLocation = new ConfigurationXMLParser()
                        .getXMLProperty("airbnb.reair.clusters.dest.hdfs.root", configFilePath)
                        .concat("_txt");

                String partitionType = textTableLocation.substring(0, textTableLocation.indexOf("://") + 3);
                //To Repair Table Name as often the metadata details are lost while doing BQ Load
                new HiveUtilities().HiveRunner("destination", "MSCK REPAIR TABLE " + tableName, configFilePath);

                Statement stmt=new HiveUtilities().getHiveConnection("destination", configFilePath);
                ResultSet allParitions = stmt.executeQuery("show partitions " + textTableName);

                while (allParitions.next()) {
                    Runtime rt = Runtime.getRuntime();
                    String completePartitions = textTableLocation.concat("/" + allParitions.getString(1));
                    LOG.info("Complete Partitions = " + completePartitions);
                    Process proc = rt.exec("hadoop fs -ls " + completePartitions);

                    BufferedReader in = new BufferedReader(new InputStreamReader(proc.getInputStream()));
                    String partition;
                    while ((partition = in.readLine()) != null) {
                        if (partition.indexOf(partitionType) == -1)
                            continue;

                        LOG.info("Partition = "+partition);
                        String timeStamp = DateTimeFormat.forPattern("yyyyMMdd_HHmmss").print(new DateTime(Calendar.getInstance()));
                        String filePath = partition.substring(partition.indexOf(partitionType), partition.length());
                        String newFilePath = filePath.concat("_" + timeStamp);
                        LOG.info("FilePath = " + filePath + "\t New File Path = " + newFilePath);
                        Process renameProcess = rt.exec("hadoop fs -mv " + filePath + " " + newFilePath);
                        renameProcess.waitFor();
                        new RenameTextTablePartitions().ErrorStream(renameProcess);
                    }
                    in.close();
                    stmt.close();
                    proc.waitFor();
                    LOG.info("Exit Value = " + proc.exitValue());
                }

                new HiveUtilities().HiveRunner("destination", "MSCK REPAIR TABLE " + tableName, configFilePath);
            }
        } catch (InterruptedException e) {
            LOG.error("RenameTextTablePartitions Main : Interrupted Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (IOException e) {
            LOG.error("RenameTextTablePartitions Main : IO Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (SQLException e) {
            LOG.error("RenameTextTablePartitions Main : SQL Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (Exception e) {
            LOG.error("RenameTextTablePartitions Main : Java lang Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
    }

    public void ErrorStream (Process proc) {
        BasicConfigurator.configure();
        LOG.info("Printing the Input Strean ....");
        BufferedReader in = new BufferedReader(new InputStreamReader(proc.getInputStream()));
        String op;
        try {
            while ((op = in.readLine()) != null) {
                LOG.info(op);
            }
            in.close();
            LOG.info("Printing the Error Strean ....");
            BufferedReader in1 = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
            while ((op = in1.readLine()) != null) {
                LOG.info(op);
            }
            in1.close();
        } catch (IOException e) {
            LOG.error("RenameTextTablePartitions Main : IO Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
    }
}