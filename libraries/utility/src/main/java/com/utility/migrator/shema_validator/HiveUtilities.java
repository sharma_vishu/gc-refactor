package com.utility.migrator.shema_validator;

import com.utility.migrator.xml_parser.ConfigurationXMLParser;

import java.io.IOException;
import java.sql.*;
import java.util.*;

//import org.apache.hive.jdbc.HiveDriver;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

public class HiveUtilities {
    private String driverName = "org.apache.hive.jdbc.HiveDriver";
    private ConfigurationXMLParser configurationXMLParser = new ConfigurationXMLParser();
    public final Logger LOG = Logger.getLogger(HiveUtilities.class);

    public Statement getHiveConnection(String tableType, String confFilePath) {
        BasicConfigurator.configure();
        Statement statement=null;
        try {
            Class.forName(driverName);
            String hiveUrl="",hiveUser="",hivePassword="";

            if (tableType.equals("source")){
                hiveUrl = configurationXMLParser.getXMLProperty("clusters.src.hive.jdbc", confFilePath);
                hiveUser = configurationXMLParser.getXMLProperty("clusters.src.hive.user", confFilePath);
                hivePassword = configurationXMLParser.getXMLProperty("clusters.src.hive.password", confFilePath);
            }
            else if (tableType.equals("destination")) {
                hiveUrl = configurationXMLParser.getXMLProperty("clusters.dest.hive.jdbc", confFilePath);
                hiveUser = configurationXMLParser.getXMLProperty("clusters.dest.hive.user", confFilePath);
                hivePassword = configurationXMLParser.getXMLProperty("clusters.dest.hive.password", confFilePath);
            }
            else {
                LOG.info("None of the configuration values caught for Hive JDBC, Hive User and Hive Password");
                System.exit(1);
            }

            Connection conn;
            conn = DriverManager.getConnection(hiveUrl, hiveUser, hivePassword);
            statement = conn.createStatement();
        } catch (ClassNotFoundException e) {
            LOG.error("HiveUtilities Main : Class Not Found Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (SQLException e) {
            LOG.error("HiveUtilities Main : SQL Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (Exception e) {
            LOG.error("HiveUtilities Main : Java lang Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
        return statement;
    }

    public void HiveRunner(String tableType, String query, String confFilePath) {
        BasicConfigurator.configure();
        try {
            Class.forName(driverName);
            String hiveUrl = "", hiveUser = "", hivePassword = "";

            if (tableType.equals("source")) {
                hiveUrl = configurationXMLParser.getXMLProperty("clusters.src.hive.jdbc", confFilePath);
                hiveUser = configurationXMLParser.getXMLProperty("clusters.src.hive.user", confFilePath);
                hivePassword = configurationXMLParser.getXMLProperty("clusters.src.hive.password", confFilePath);
            } else if (tableType.equals("destination")) {
                hiveUrl = configurationXMLParser.getXMLProperty("clusters.dest.hive.jdbc", confFilePath);
                hiveUser = configurationXMLParser.getXMLProperty("clusters.dest.hive.user", confFilePath);
                hivePassword = configurationXMLParser.getXMLProperty("clusters.dest.hive.password", confFilePath);
            } else {
                LOG.info("None of the configuration values caught for Hive JDBC, Hive User and Hive Password");
                System.exit(1);
            }

            Connection con = DriverManager.getConnection(hiveUrl, hiveUser, hivePassword);
            Statement stmt = con.createStatement();

            LOG.info("Executing Hive Query = " + query);

            stmt.execute(query);
            stmt.close();
            con.close();
        } catch (ClassNotFoundException e) {
            LOG.error("HiveUtilities Main : Class Not Found Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (SQLException e) {
            LOG.error("HiveUtilities Main : SQL Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (Exception e) {
            LOG.error("HiveUtilities Main : Java lang Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
    }

    public void HiveIteratorList(String tableType, Iterator item, String confFilePath)  {
        BasicConfigurator.configure();
        while(item.hasNext()){
            String qry = item.next().toString();
            HiveRunner(tableType, qry, confFilePath);
        }
    }

    public String getTableSchema(String tableType, String hiveTblName, String confFilePath) {
        BasicConfigurator.configure();
        String tableDef="";
        try {
            Statement st = getHiveConnection(tableType, confFilePath);
            LOG.info("show create table " + hiveTblName);
            ResultSet rs = st.executeQuery("show create table " + hiveTblName);


            while (rs.next()) {
                if (rs.getString(1).equals("LOCATION"))
                    break;
                else
                    tableDef += rs.getString(1) + " ";
            }
            st.close();
        } catch (SQLException e) {
            LOG.error("HiveUtilities Main : SQL Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
        return tableDef;
    }

    public Map<String,ArrayList<String>> hiveSchemaDetails(String tableType, String query, String confFilePath) {
        BasicConfigurator.configure();
        String hiveUrl = "", hiveUser = "", hivePassword = "";
        Map<String, ArrayList<String>> ret_values = new HashMap();
        try {
            if (tableType.equals("source")) {
                hiveUrl = configurationXMLParser.getXMLProperty("clusters.src.hive.jdbc", confFilePath);
                hiveUser = configurationXMLParser.getXMLProperty("clusters.src.hive.user", confFilePath);
                hivePassword = configurationXMLParser.getXMLProperty("clusters.src.hive.password", confFilePath);
            } else if (tableType.equals("destination")) {
                hiveUrl = configurationXMLParser.getXMLProperty("clusters.dest.hive.jdbc", confFilePath);
                hiveUser = configurationXMLParser.getXMLProperty("clusters.dest.hive.user", confFilePath);
                hivePassword = configurationXMLParser.getXMLProperty("clusters.dest.hive.password", confFilePath);
            } else {
                LOG.info("None of the configuration values caught for Hive JDBC, Hive User and Hive Password");
                System.exit(1);
            }
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(hiveUrl, hiveUser, hivePassword);
            Statement stmt = con.createStatement();

            ArrayList<String> columns = new ArrayList<>();
            ArrayList<String> datatypes = new ArrayList<>();

            LOG.info("Executing Query = " + query);
            ResultSet rs = stmt.executeQuery(query);

            ResultSetMetaData metaData = rs.getMetaData();

            //To get the total column count
            int columnCount = metaData.getColumnCount();
            LOG.info("Column count = "+columnCount);

            while (rs.next()) {
                if (rs.getString(metaData.getColumnName(2)) == null) {
                    break;
                }
                columns.add(rs.getString(metaData.getColumnName(1)));
                datatypes.add(rs.getString(metaData.getColumnName(2)));
            }
            ret_values.put("columns", columns);
            ret_values.put("data_types", datatypes);
        stmt.close();
        con.close();
        } catch (ClassNotFoundException e) {
            LOG.error("HiveUtilities Main : Class Not Found Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (Exception e) {
            LOG.error("HiveUtilities Main : Java lang Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
        return ret_values;
    }

    public Boolean validateHiveTableExists (String tableType, String hiveTableName, String confFilePath) {
        BasicConfigurator.configure();
        String hiveUrl, hiveUser, hivePassword;
        Boolean bool=false;
        try {
            if (tableType.equals("destination")) {
                hiveUrl = configurationXMLParser.getXMLProperty("clusters.dest.hive.jdbc", confFilePath);
                hiveUser = configurationXMLParser.getXMLProperty("clusters.dest.hive.user", confFilePath);
                hivePassword = configurationXMLParser.getXMLProperty("clusters.dest.hive.password", confFilePath);
            } else {
                hiveUrl = configurationXMLParser.getXMLProperty("clusters.dest.hive.jdbc", confFilePath);
                hiveUser = configurationXMLParser.getXMLProperty("clusters.dest.hive.user", confFilePath);
                hivePassword = configurationXMLParser.getXMLProperty("clusters.dest.hive.password", confFilePath);
            }
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(hiveUrl, hiveUser, hivePassword);
            DatabaseMetaData databaseMetaData = con.getMetaData();
            ResultSet resultSet = databaseMetaData.getTables(null, hiveTableName.substring(0, hiveTableName.indexOf(".")),
                    hiveTableName.substring(hiveTableName.indexOf(".") + 1, hiveTableName.length()), null);

            if (!resultSet.next())
                bool= false;
            else
                bool= true;
            con.close();
        } catch (ClassNotFoundException e) {
            LOG.error("HiveUtilities Main : Class Not Found Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (SQLException e) {
            LOG.error("HiveUtilities Main : SQL Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (Exception e) {
            LOG.error("HiveUtilities Main : Java lang Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
        return bool;
    }
}
