package com.utility.migrator.sync_utility;

import com.utility.migrator.xml_parser.ConfigurationXMLParser;
import org.slf4j.LoggerFactory;

import java.sql.*;

/**
 * Created by tkmah7u on 7/3/17.
 */
public class HiveConnection {
    private static ConfigurationXMLParser configurationXMLParser = new ConfigurationXMLParser();
    static org.slf4j.Logger LOG = LoggerFactory.getLogger(IncrementalHiveTableAutomator.class);
    public static void main(String[] args) {
    try{

        Class.forName("org.apache.hive.jdbc.HiveDriver");
        String hiveUrl, hiveUser, hivePassword,confFilePath=args[0];
        hiveUrl = configurationXMLParser.getXMLProperty("clusters.dest.hive.jdbc", confFilePath);
        hiveUser = configurationXMLParser.getXMLProperty("clusters.dest.hive.user", confFilePath);
        hivePassword = configurationXMLParser.getXMLProperty("clusters.dest.hive.password", confFilePath);
        Connection con = DriverManager.getConnection(hiveUrl, hiveUser, hivePassword);
        Statement stmt = con.createStatement();


        ResultSet rs = stmt.executeQuery("show create table adityadb1.final_phase_avro_poc_part2");

        LOG.info("show create table adityadb1.final_phase_avro_poc_part2");

        String tableDef = "", tblLocation = "";
        while (rs.next()) {
            if (rs.getString(1).equals("LOCATION")) {
                rs.next();
                tblLocation = rs.getString(1);
                LOG.info("TableLocation = " + tblLocation);
                break;
            } else
                tableDef += rs.getString(1) + " ";
        }
        System.out.println(tableDef);
    }catch (ClassNotFoundException e) {
        e.printStackTrace();

    } catch (SQLException e) {
        e.printStackTrace();
    }
    }}
