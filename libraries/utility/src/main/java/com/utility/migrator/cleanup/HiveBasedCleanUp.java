package com.utility.migrator.cleanup;

import com.utility.migrator.initializer.SQLUtility;
import com.utility.migrator.shema_validator.HiveUtilities;
import com.utility.migrator.xml_parser.ConfigurationXMLParser;
import org.apache.commons.cli.*;
import org.apache.hive.jdbc.HiveConnection;
import org.apache.log4j.BasicConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.Statement;
import java.util.Arrays;

public class HiveBasedCleanUp {
    static Logger LOG = LoggerFactory.getLogger(HiveBasedCleanUp.class);
    public static void main(String[] args) {
        BasicConfigurator.configure();
        Options options = new Options();

        Option flow_name = new Option("f", "flow_name", true, "flow name");
        Option table_name = new Option("t", "sync_table_name", true, "sync table name");
        Option config_file_path = new Option("c", "config_file_path", true, "config file path");

        flow_name.setRequired(true);
        table_name.setRequired(true);
        config_file_path.setRequired(true);

        options.addOption(flow_name);
        options.addOption(table_name);
        options.addOption(config_file_path);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            LOG.error("HiveBasedCleanUp : Parse exception met "+e.getMessage());
            formatter.printHelp("utility-name", options);
            System.exit(1);
            return;
        }

        SQLUtility sqlUtility = new SQLUtility();
        ConfigurationXMLParser configurationXMLParser = new ConfigurationXMLParser();

        String flowName = cmd.getOptionValue("flow_name");
        String tableName = cmd.getOptionValue("sync_table_name");
        String confFilePath = cmd.getOptionValue("config_file_path");

        try {
            Statement stmt= new HiveUtilities().getHiveConnection("destination", confFilePath);

            stmt.executeQuery("DROP TABLE IF EXISTS " + tableName.concat("_text"));

            stmt.executeQuery("DROP TABLE IF EXISTS " + tableName.concat("_incremental"));

            stmt.executeQuery("DROP TABLE IF EXISTS " + tableName.concat("_incremental"));

            stmt.close();
        } catch (Exception e) {
            LOG.error("HiveBasedCleanup Main : Java Lang Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
    }
}
