package com.utility.migrator.shema_validator;

import com.utility.migrator.initializer.SQLUtility;
import com.utility.migrator.xml_parser.ConfigurationXMLParser;
import org.apache.commons.cli.*;
import org.apache.log4j.BasicConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.*;
import java.util.*;

public class SchemaValidator {
    private static String driverName = "org.apache.hive.jdbc.HiveDriver";
    static Logger LOG= LoggerFactory.getLogger(SchemaValidator.class);
    static Map<String, String> bigQueryColumns = new HashMap<>();
    static ArrayList<String> bigQueryChangedKey = new ArrayList<>();
    static int bigQuerybatchFlag = -1;
    
    private ConfigurationXMLParser configurationXMLParser = new ConfigurationXMLParser();

    public static void main(String[] args) {
        BasicConfigurator.configure();
        LOG.info("Started on the path to read the configuration files");
        Options options = new Options();

        Option table_name = new Option("t", "sync_table_name", true, "sync table name");
        Option config_file_path = new Option("c", "config_file_path", true, "config file path");

        table_name.setRequired(true);
        config_file_path.setRequired(true);

        options.addOption(table_name);
        options.addOption(config_file_path);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            LOG.error("SchemaValidator : Parse Exception met"+e.getMessage());
            formatter.printHelp("utility-name", options);

            System.exit(1);
            return;
        }

        ArrayList<String> src_columns, dest_columns, src_datatypes, dest_datatypes;
        //ToDO destTableName (IN CASE OF diffrent table name on destination side
        //Accepting User arguments
        String fileTableName = cmd.getOptionValue("sync_table_name");
        String destTableName = cmd.getOptionValue("sync_table_name");
        String confFilePath = cmd.getOptionValue("config_file_path");


        LOG.info("Schema Validation started for " + fileTableName);

        int indexedTable = fileTableName.indexOf('.');
        String dbName = fileTableName.substring(0, indexedTable);
        String syncTableName = fileTableName.substring(indexedTable + 1, fileTableName.length());

        //Destination Temp Db
        new HiveUtilities().HiveRunner("destination", "CREATE DATABASE IF NOT EXISTS ".concat(dbName), confFilePath);

        //TODO put try catch block and add logger info
        LOG.info("Establishing Hive Table at the Destination");

        try {
            new SchemaValidator().DestClusterSetup(fileTableName, confFilePath);
        /*
        Recreating the Staging Table
        */
            new BigQueryUtilities().automatedBQSchemaGeneratorForStaging(fileTableName, confFilePath);
            LOG.info("Staging Table Schema Recreated.....");

        /*
        Validate if the table is batch or incremental.
        */

            String sqlDbName = new ConfigurationXMLParser().getXMLProperty("clusters.mysql.db.name", confFilePath);
            String sqlTableName = new ConfigurationXMLParser().getXMLProperty("clusters.mysql.flow.table.name", confFilePath);
            String mySqlTableName = sqlDbName.concat(".").concat(sqlTableName);

            String checkFlowExistsString = "select count(*) from " + mySqlTableName + " where table_name='" + fileTableName + "' and status='SUCCESSFUL'";
            LOG.info("Check flow exists string = "+checkFlowExistsString);
            int count = new SQLUtility().getCount(checkFlowExistsString, confFilePath);

            if (count != 0) {
                LOG.info("Big Query Schema Already Exists, Ain't Recreating");
            } else {
                //For the Original Table
                new BigQueryUtilities().automatedBQSchemaGenerator(fileTableName, confFilePath);
                LOG.info("Big Query Schema & Staging Table Schema Successfully created....");
                bigQuerybatchFlag = 1;
            }

        /*
        Source Schema will be taken from the source table from source cluster while Destination schema will be taken from persisted storage.
        */
            Map<String, ArrayList<String>> column_details_src = new HiveUtilities().hiveSchemaDetails("source", "describe ".concat(fileTableName), confFilePath);
            src_columns = column_details_src.get("columns");
            src_datatypes = column_details_src.get("data_types");

            //Fetching relevenat details from the destination clusters
            Map<String, ArrayList<String>> column_details_dest = new HiveUtilities().hiveSchemaDetails("destination", "describe ".concat(destTableName), confFilePath);
            dest_columns = column_details_dest.get("columns");
            dest_datatypes = column_details_dest.get("data_types");

            int result = new SchemaValidator().comparator(src_columns, src_datatypes, dest_columns, dest_datatypes, fileTableName, destTableName, confFilePath);

            if (result == -1) {
                LOG.info("Schema already up to date. No schema change involved.");
                LOG.info("Schema already up to date. No schema change involved.");
            } else if (result == 1) {
                LOG.info("Changes between source and destination successfully implemented");
                LOG.info("Changes between source and destination successfully implemented");
            } else {
                LOG.info("SchemaValidator result : "+result);
                LOG.info("Something is wrong ......");
            }

        /*
        Time to update bigQuery columns
        */
            if (bigQueryColumns.size() > 0 && result != -1 && bigQuerybatchFlag == -1) {
                String consoleDump = "Total independent big query tasks to achieve = " + bigQueryColumns.size();
                LOG.info(consoleDump);
                // Directly recreate the new schema
                new BigQueryUtilities().updateBigQueryTable(syncTableName, confFilePath);
            }
            LOG.info("Schema Validation completed Successfully.....");
        } catch (Exception e) {
            LOG.error("SchemaValidator Main : Java lang Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }

    }

    public int comparator(ArrayList<String> src_col, ArrayList<String> src_dt, ArrayList<String> dest_col,
                                 ArrayList<String> dest_dt, String syncTableName, String destTableName, String confFilePath)
            throws Exception {
        BasicConfigurator.configure();
        ArrayList<String> runners = new ArrayList<>();

        if (src_col.equals(dest_col) && src_dt.equals(dest_dt)) {
            LOG.info("Both source and destination schema are in sync");
            return -1;
        }
        else {
            ArrayList<String> src_col_duplicate = duplicateArrayList(src_col);

            //Keep a copy of the source column.
            src_col_duplicate.removeAll(dest_col);
            Iterator diff_source = src_col_duplicate.iterator();

            String add_column = "";
            while (diff_source.hasNext()) {
                String colName = diff_source.next().toString();
                int indexOf = src_col.indexOf(colName);
                String dataType = src_dt.get(indexOf);

                //TODO : Remove this Sout
                LOG.info("Col Name Added - "+colName+" DataType = "+dataType);
                bigQueryChangedKey.add(colName);
                bigQueryColumns.put(colName, dataType);
                add_column += colName + " " + dataType + ",";
            }

            LOG.info("Printing add column details");
            if (add_column.length() != 0) {
                add_column = add_column.substring(0,(add_column.length()-1));
                LOG.info("add_column = "+add_column);
                String qry = "ALTER TABLE " + destTableName + " ADD COLUMNS (" + add_column + ")";
                LOG.info("Query = "+qry);
                runners.add(qry);
            }

        }
        //Addition of Hive Columns query
        new HiveUtilities().HiveIteratorList("destination",runners.iterator(), confFilePath);
        return 1;
    }

    public ArrayList<String> duplicateArrayList(ArrayList<String> list) {
        ArrayList<String> clone = new ArrayList<String>(list.size());
        int i = 0;
        for (String item : list) clone.add(list.get(i++));
        return clone;
    }

    public void DestClusterSetup (String fromTableName, String confFilePath)  {
        BasicConfigurator.configure();
        try {
        if (new HiveUtilities().validateHiveTableExists("destination", fromTableName, confFilePath))
            LOG.info("Table " + fromTableName + " already exists in the destination. Exiting destination cluster setup....");
        else {
            //new HiveUtilities().HiveRunner("destination","DROP TABLE IF EXISTS " + fromTableName, confFilePath );
            String srcTableSchema = new HiveUtilities().getTableSchema("source", fromTableName, confFilePath);
            srcTableSchema += "\t" + "LOCATION '" + configurationXMLParser.getXMLProperty("airbnb.reair.clusters.dest.hdfs.root", confFilePath) + "'";

            String filteredString = "CREATE EXTERNAL".concat(
                    srcTableSchema.substring(srcTableSchema.toLowerCase().indexOf("external") + 8, srcTableSchema.length()));
            LOG.info("Table Schema = " + filteredString);

            /*
            Setting up hive runner table in the destination Hive
            */
            new HiveUtilities().HiveRunner("destination", filteredString, confFilePath);
        }
        } catch (Exception e) {
            LOG.error("SchemaValidator Main : Java lang Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
    }
}
