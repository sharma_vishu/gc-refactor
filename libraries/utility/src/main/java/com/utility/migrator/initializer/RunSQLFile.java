package com.utility.migrator.initializer;

import org.apache.commons.cli.*;

import java.io.*;
import java.sql.*;
import java.util.Arrays;
import java.util.Properties;
import com.mysql.jdbc.Driver;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

public class RunSQLFile {
    public static Logger LOG = Logger.getLogger(RunSQLFile.class);

    public static void main(String[] args) {
        BasicConfigurator.configure();
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (ClassNotFoundException e) {
            LOG.error("RunSQLFile Main : CLass not found Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (IllegalAccessException e) {
            LOG.error("RunSQLFile Main : Illegal Access Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (InstantiationException e) {
            LOG.error("RunSQLFile Main : Instatiation Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }

        Options options = new Options();
        Option sql_file_path = new Option("f", "sql_file_path", true, "sql file path");
        Option property_file_path = new Option("p", "property_file_path", true, "property file path");
        sql_file_path.setRequired(true);
        property_file_path.setRequired(true);
        options.addOption(sql_file_path);
        options.addOption(property_file_path);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            LOG.info("RunSQLFile : Parse Exception met "+ e.getMessage());
            formatter.printHelp("utility-name", options);

            System.exit(1);
            return;
        }

        String sqlFile = cmd.getOptionValue("sql_file_path");
        String propertiesFile = cmd.getOptionValue("property_file_path");

        try {
            //Get the mysql file to be executed under sql schema
            BufferedReader br = new BufferedReader(new FileReader(new File(sqlFile)));
            Properties configurations = new Properties();
            configurations.load(new FileInputStream(new File(propertiesFile)));

            String sql_schema = "", line;
            while ((line = br.readLine()) != null)
                sql_schema += line + " ";

            String sqlJDBC = configurations.getProperty("TVAR_SQL_JDBC");
            String sqlUser = configurations.getProperty("TVAR_SQL_USER");
            String sqlPassword = configurations.getProperty("TVAR_SQL_PASSWORD");

            String sql_db_name = configurations.getProperty("TVAR_SQL_DB_NAME");

            Connection connection = DriverManager.getConnection(sqlJDBC, sqlUser, sqlPassword);
            Statement statement = connection.createStatement();

            statement.execute("CREATE DATABASE IF NOT EXISTS " + sql_db_name);
            //Executing the Create Table
            statement.execute(sql_schema.replaceFirst("DB_NAME", sql_db_name));
            statement.close();
            connection.close();
        } catch (SQLException e) {
            LOG.error("RunSQLFile Main : SQL Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (FileNotFoundException e) {
            LOG.error("RunSQLFile Main : File Not Found Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (IOException e) {
            LOG.error("RunSQLFile Main : IO Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
    }
}
