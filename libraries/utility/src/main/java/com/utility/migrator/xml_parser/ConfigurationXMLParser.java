package com.utility.migrator.xml_parser;

import org.apache.log4j.BasicConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class ConfigurationXMLParser {
    final Logger LOG= LoggerFactory.getLogger(ConfigurationXMLParser.class);

    public String getXMLProperty(String propertyName, String fileName)  {
        BasicConfigurator.configure();
        Document document=null;
        try {
            document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new File(fileName));
        } catch (SAXException e) {
            LOG.error("ConfigurationXMLParser Main : SAX Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (ParserConfigurationException e) {
            LOG.error("ConfigurationXMLParser Main : Parser Configuration Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (IOException e) {
            LOG.error("ConfigurationXMLParser Main : IO Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
        document.getDocumentElement().normalize();

        NodeList nList = document.getElementsByTagName("property");

        String propertyValue ="";

        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);

            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                if (!eElement.getElementsByTagName("name").item(0).getTextContent().equals(propertyName))
                    continue;

                propertyValue = eElement.getElementsByTagName("value").item(0).getTextContent();
            }
        }

        return propertyValue;
    }
}
