package com.utility.migrator.shema_validator;

import com.utility.migrator.xml_parser.ConfigurationXMLParser;
import org.apache.log4j.BasicConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.text.StyledEditorKit;
import java.sql.*;
import java.util.Arrays;

public class SqlUtilities {
    private ConfigurationXMLParser configurationXMLParser = new ConfigurationXMLParser();
    static Logger LOG= LoggerFactory.getLogger(SqlUtilities.class);

    public Connection getSQLConnection(String confFilePath) {
        BasicConfigurator.configure();
        Connection connection=null;
        try {

            //TODO : For MSQL Connection
            String jdbcUrl = configurationXMLParser.getXMLProperty("clusters.mysql.host", confFilePath);
            String sqlUser = configurationXMLParser.getXMLProperty("clusters.mysql.user", confFilePath);
            String sqlPassword = configurationXMLParser.getXMLProperty("clusters.mysql.password", confFilePath);

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            connection = DriverManager.getConnection(jdbcUrl, sqlUser, sqlPassword);
            return connection;

        } catch (IllegalAccessException e) {
            LOG.error("SqlUtilities Main : Illegal Access Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (InstantiationException e) {
            LOG.error("SqlUtilities Main : Instatiation Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (ClassNotFoundException e) {
            LOG.error("SqlUtilities Main : Class Not Found Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (SQLException e) {
            LOG.error("SqlUtilities Main : SQL Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (Exception e) {
            LOG.error("SqlUtilities Main : Java lang Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
        return connection;
    }

    public boolean updatePersistantSchema(String hiveTableName, String ddhSchema, String dataProcSchema,
                                                 String bigQuerySchema, String confFilePath)  {
        Boolean result=false;
        BasicConfigurator.configure();
        try {
            String sqlDbName = configurationXMLParser.getXMLProperty("clusters.mysql.db.name", confFilePath);
            String sqlTableName = configurationXMLParser.getXMLProperty("clusters.mysql.schema.table.name", confFilePath);
            String mySqlTable = sqlDbName + "." + sqlTableName;
            Connection conn= getSQLConnection(confFilePath);
            Statement st = conn.createStatement();

            String updateTable = "UPDATE " + mySqlTable + " SET ddh_schema=\"" + ddhSchema + "\", dataproc_schema=\""
                    + dataProcSchema + "\", bigquery_schema='" + bigQuerySchema + "' WHERE table_name=\"" + hiveTableName + "\"";

            LOG.info("UpdateTable = "+updateTable);

            result = st.execute(updateTable);
            LOG.info("result = "+result);
            if (!result) {
                LOG.info("Schema Update failed ....");
            } else
                LOG.info("Schema update successful ....");
            st.close();
            conn.close();
        } catch (SQLException e) {
            LOG.error("SqlUtilities Main : SQL Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (Exception e) {
            LOG.error("SqlUtilities Main : Java lang Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
        return result;
    }

    public boolean persistantStorage(String tableName, String ddhSchema, String dataProcSchema, String bigQuery,
                                            String confFilePath)  {
        Boolean outcome=false;
        BasicConfigurator.configure();
        try {
            String sqlDbName = configurationXMLParser.getXMLProperty("clusters.mysql.db.name", confFilePath);
            String sqlTableName = configurationXMLParser.getXMLProperty("clusters.mysql.schema.table.name", confFilePath);
            Connection conn= getSQLConnection(confFilePath);
            Statement stmt = conn.createStatement();
            String mySqlTableName = sqlDbName.concat(".").concat(sqlTableName);

            stmt.execute("CREATE DATABASE IF NOT EXISTS " + sqlDbName);
            stmt.execute("CREATE TABLE IF NOT EXISTS " + mySqlTableName + " (table_name varchar(300), ddh_schema varchar(2000), dataproc_schema varchar(2000), bigquery_schema varchar(2000) );");

            String values = "\"" + tableName + "\",\"" + ddhSchema + "\",\"" + dataProcSchema + "\",\'" + bigQuery + "\'";

            String sql = "INSERT INTO " + mySqlTableName + " (table_name,ddh_schema,dataproc_schema,bigquery_schema) VALUES (" +
                    values + ")";

            outcome = stmt.execute(sql);
            stmt.close();
            conn.close();
        } catch (SQLException e) {
            LOG.error("SqlUtilities Main : SQL Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (Exception e) {
            LOG.error("SqlUtilities Main : Java lang Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
        return outcome;
    }

    public boolean checkSQLTableExists(String recordTableName, String confFilePath) {
        BasicConfigurator.configure();
        Boolean bool = false;
        try {
            String mySqlDbName = configurationXMLParser.getXMLProperty("clusters.mysql.db.name", confFilePath);
            String mySqlFlowTableName = configurationXMLParser.getXMLProperty("clusters.mysql.flow.table.name", confFilePath);

            String mySqlTableName = mySqlDbName.concat(".").concat(mySqlFlowTableName);

            Connection conn= getSQLConnection(confFilePath);
            Statement stmt = conn.createStatement();
            String checkTableQuery = "select count(*) from " + mySqlTableName + " where table_name=\"" + recordTableName + "\"" +
                    " and (status != 'RUNNING' OR job_stage NOT IN ('Stage 1 : Job Initialization'))";
            LOG.info("Check Table Query = "+checkTableQuery);
            ResultSet rs = stmt.executeQuery(checkTableQuery);

            int count = 0;
            while (rs.next()) {
                count = rs.getInt(1);
            }

            if (count == 0)
                bool = false;
            else
                bool = true;
            stmt.close();
            conn.close();
        } catch (SQLException e) {
            LOG.error("SqlUtilities Main : SQL Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (Exception e) {
            LOG.error("SqlUtilities Main : Java lang Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
    return bool;
    }
}
