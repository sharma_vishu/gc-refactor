package com.utility.migrator.sync_utility;

import com.mysql.jdbc.log.Log;
import com.utility.migrator.xml_parser.ConfigurationXMLParser;
import org.apache.commons.cli.*;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.util.Progressable;
import org.apache.log4j.BasicConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.*;
import java.util.Arrays;

import static java.lang.System.out;

public class HDFSFileWriter {
    private static ConfigurationXMLParser configurationXMLParser = new ConfigurationXMLParser();
    private static String driverName = "org.apache.hive.jdbc.HiveDriver";
    static Logger LOG= LoggerFactory.getLogger(HDFSFileWriter.class);

    public static void main(String[] args)  {
        BasicConfigurator.configure();
        Options options = new Options();

        Option table_name = new Option("t", "sync_table_name", true, "sync table name");
        Option config_file_path = new Option("c", "config_file_path", true, "config file path");

        table_name.setRequired(true);
        config_file_path.setRequired(true);

        options.addOption(table_name);
        options.addOption(config_file_path);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            LOG.error("HDFSFileWriter : Parse Exception met"+e.getMessage());
            formatter.printHelp("utility-name", options);

            System.exit(1);
            return;
        }

        String sourceTableName = cmd.getOptionValue("sync_table_name");
        String confFile = cmd.getOptionValue("config_file_path");

        try {
            writeToHdfs(confFile, sourceTableName);
        } catch (Exception e) {
            LOG.error("HDFSFileWriter Main : Java lang Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
    }

    public static void writeToHdfs (String confFilePath, String sourceTableName)  {
        BasicConfigurator.configure();
        try {
            String tableLocation = getTableSchema(sourceTableName, confFilePath).replace("'", "").trim();
            LOG.info("TableLocation = "+tableLocation);

            String hdfsURI = tableLocation.substring(0, tableLocation.indexOf("/", tableLocation.lastIndexOf(":")));

            Configuration configuration = new Configuration();
            FileSystem hdfs = FileSystem.get(new URI(hdfsURI), configuration);
            Path file = new Path(hdfsURI.concat("/migrator/").concat(sourceTableName));
            if (hdfs.exists(file)) {
                hdfs.delete(file, true);
            }
            OutputStream os = hdfs.create(file,
                    new Progressable() {
                        public void progress() {
                            out.println("...bytes written: []");
                        }
                    });
            BufferedWriter br = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            br.write(sourceTableName);
            br.close();
            hdfs.close();
        } catch (UnsupportedEncodingException e) {
            LOG.error("HDFSFileWriter Main : Unsupported Encoding Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (URISyntaxException e) {
            LOG.error("HDFSFileWriter Main : URI Syntax Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (IOException e) {
            LOG.error("HDFSFileWriter Main : IO Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (Exception e) {
            LOG.error("HDFSFileWriter Main : Java lang Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
    }

    public static String getTableSchema(String hiveTblName, String confFilePath) {
        BasicConfigurator.configure();
        String tblLocation="";
        try {
            Class.forName(driverName);

            String hiveJdbcURL = configurationXMLParser.getXMLProperty("clusters.dest.hive.jdbc", confFilePath);
            String hiveUser = configurationXMLParser.getXMLProperty("clusters.dest.hive.user", confFilePath);
            String hivePassword = configurationXMLParser.getXMLProperty("clusters.dest.hive.password", confFilePath);

            Connection con = DriverManager.getConnection(hiveJdbcURL, hiveUser, hivePassword);
            Statement stmt = con.createStatement();

            ResultSet rs = stmt.executeQuery("show create table " + hiveTblName);


            while (rs.next()) {
                if (rs.getString(1).equals("LOCATION")) {
                    rs.next();
                    tblLocation = rs.getString(1);
                    break;
                }
            }
            stmt.close();
            con.close();
        } catch (SQLException e) {
            LOG.error("HDFSFileWriter Main : SQL Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (ClassNotFoundException e) {
            LOG.error("HDFSFileWriter Main : Class Not Found Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (Exception e) {
            LOG.error("HDFSFileWriter Main : Java lang Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
        return tblLocation;
    }
}
