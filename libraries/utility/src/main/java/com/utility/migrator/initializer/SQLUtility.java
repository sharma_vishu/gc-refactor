package com.utility.migrator.initializer;

import com.utility.migrator.xml_parser.ConfigurationXMLParser;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.Arrays;

public class SQLUtility {
    ConfigurationXMLParser configurationXMLParser = new ConfigurationXMLParser();
    public final Logger LOG = Logger.getLogger(SQLUtility.class);

    public Connection getSQLConnection(String confFilePath) {
        BasicConfigurator.configure();
        Connection connection=null;
        try {
            //TODO : For MYSQL Connection
            String jdbcUrl = configurationXMLParser.getXMLProperty("clusters.mysql.host", confFilePath);
            String sqlUser = configurationXMLParser.getXMLProperty("clusters.mysql.user", confFilePath);
            String sqlPassword = configurationXMLParser.getXMLProperty("clusters.mysql.password", confFilePath);

            Class.forName("com.mysql.jdbc.Driver").newInstance();

            connection = DriverManager.getConnection(jdbcUrl, sqlUser, sqlPassword);

        } catch (SQLException e){
            LOG.error("SQLUtility Main : SQL Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }catch (InstantiationException e) {
            LOG.error("SQLUtility Main : Instatiation Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (IllegalAccessException e) {
            LOG.error("SQLUtility Main : Illegal Access Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));        } catch (ClassNotFoundException e) {
        } catch (Exception e) {
            LOG.error("SQLUtility Main : Java lang Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
    return connection;
    }

    public int getCount (String queryString, String confFilePath) {
        BasicConfigurator.configure();
        int count=-1;
        try {
            Connection con = new SQLUtility().getSQLConnection(confFilePath);
            Statement stmt = con.createStatement();
            ResultSet checkFlowExists = stmt.executeQuery(queryString);
            while (checkFlowExists.next()) {
                count = checkFlowExists.getInt(1);
            }
            stmt.close();
            con.close();
        } catch (SQLException e) {
            LOG.error("SQLUtility Main : SQL Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
    return count;
    }
}
