package com.trigger.utilities;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;

public class SQLUtilities {
    public final Logger LOG = Logger.getLogger(SQLUtilities.class);
    public String getSyncTableName (String projectId, String flowId, String jobId, String confFilePath) {
        BasicConfigurator.configure();
        Properties configurations = new Properties();
        try {
            configurations.load(new FileInputStream(confFilePath));
        } catch (FileNotFoundException fnfe) {
            LOG.error("SQLUtilities Main : No valid configuration file path found ....");
            LOG.error(Arrays.toString(fnfe.getStackTrace()));
        } catch (IOException ioe) {
            LOG.error("SQLUtilities Main : IO Exception met ....");
            LOG.error(Arrays.toString(ioe.getStackTrace()));
        }
        String sync_table = "";
        try {
            Connection connection = new SQLUtilities()
                    .getSQLConnection("CLOUD_CLUSTER", confFilePath);
            Statement fetchStatement = connection.createStatement();
            String fetchTableTemplate = "SELECT sync_table FROM CLOUD_DICT_TBL_NAME WHERE project_id = 'PROJECT_ID' " +
                    "and flow_id = 'FLOW_ID' and job_id = 'JOB_ID';";
            String fetchTable = fetchTableTemplate
                    .replace("PROJECT_ID", projectId)
                    .replace("FLOW_ID", flowId)
                    .replace("JOB_ID", jobId)
                    .replace("CLOUD_DICT_TBL_NAME", configurations.getProperty("CLOUD_DICT_TBL_NAME"));

            ResultSet fetchTableResultSet = fetchStatement.executeQuery(fetchTable);

            while (fetchTableResultSet.next()) {
                sync_table = fetchTableResultSet.getString(1);
            }
            fetchStatement.close();
            connection.close();
        } catch (SQLException sqle) {
            LOG.error("SQLUtilities Main : SQL Exception met ....");
            LOG.error(Arrays.toString(sqle.getStackTrace()));
        } catch (Exception e) {
            LOG.error("SQLUtilities Main : Java Lang Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
        return sync_table;
    }

    public int isTheJobMonitored (String projectId, String flowId, String jobId, String confFilePath) {
        BasicConfigurator.configure();
        Properties configurations = new Properties();
        try {
            configurations.load(new FileInputStream(confFilePath));
        } catch (FileNotFoundException fnfe) {
            LOG.error("SQLUtilities Main : No valid configuration file path found ....");
            LOG.error(Arrays.toString(fnfe.getStackTrace()));
        } catch (IOException ioe) {
            LOG.error("SQLUtilities Main : IO Exception met ....");
            LOG.error(Arrays.toString(ioe.getStackTrace()));
        }
        int count = 0;
        try {
            Connection connection = new SQLUtilities()
                    .getSQLConnection("CLOUD_CLUSTER", confFilePath);
            Statement monitoringStatement = connection.createStatement();
            String countQueryTemplate = "SELECT count(*) FROM CLOUD_DICT_TBL_NAME WHERE project_id = 'PROJECT_ID' " +
                    "and flow_id = 'FLOW_ID' and job_id = 'JOB_ID';";

            String countQuery = countQueryTemplate
                    .replace("PROJECT_ID", projectId)
                    .replace("FLOW_ID", flowId)
                    .replace("JOB_ID", jobId)
                    .replace("CLOUD_DICT_TBL_NAME", configurations.getProperty("CLOUD_DICT_TBL_NAME"));

            ResultSet monitoringResultSet = monitoringStatement.executeQuery(countQuery);

            while (monitoringResultSet.next()) {
                count = monitoringResultSet.getInt(1);
            }
            monitoringStatement.close();
            connection.close();
        } catch (SQLException sqle) {
            LOG.error("SQLUtilities Main : SQL Exception met ....");
            LOG.error(Arrays.toString(sqle.getStackTrace()));
        } catch (Exception e) {
            LOG.error("SQLUtilities Main : Java Lang Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
        return count;
    }

    public String getProjectName (int projectId, String confFilePath) {
        BasicConfigurator.configure();
        LOG.info("projectId=" + projectId);
        String query = "select name from azkaban.projects where id=" + projectId;
        String projectName=null;
        try {
            Connection connection = new SQLUtilities()
                    .getSQLConnection("DDH_CLUSTER", confFilePath);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            //TODO remove this default once it works

            if (resultSet.next()) {
                do {
                    LOG.info("projectName = " + resultSet.getString(1));
                    projectName = resultSet.getString(1);
                } while (resultSet.next());
            } else
                projectName = null;

            statement.close();
            connection.close();
        } catch (SQLException sqle) {
            LOG.error("SQLUtilities Main : SQL Exception met ....");
            LOG.error(Arrays.toString(sqle.getStackTrace()));
        } catch (Exception e) {
            LOG.error("SQLUtilities Main : Java Lang Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
        return projectName;
    }

    public Connection getSQLConnection(String type, String confFilePath) {
            BasicConfigurator.configure();
            Properties configurations = new Properties();
            try {
                configurations.load(new FileInputStream(confFilePath));
            } catch (FileNotFoundException fnfe) {
                LOG.error("SQLUtilities Main : No valid configuration file path found ....");
                LOG.error(Arrays.toString(fnfe.getStackTrace()));
            } catch (IOException ioe) {
                LOG.error("SQLUtilities Main : IO Exception met ....");
                LOG.error(Arrays.toString(ioe.getStackTrace()));
            }

            String sqlJDBC="", sqlUser="", sqlPassword="";
            if (type.equals("DDH_CLUSTER")){

                sqlJDBC = configurations.getProperty("DDH_JDBC");
                sqlUser = configurations.getProperty("DDH_USER");
                sqlPassword = configurations.getProperty("DDH_PASSWORD");

            } else if (type.equals("CLOUD_CLUSTER")) {

                sqlJDBC = configurations.getProperty("CLOUD_JDBC");
                sqlUser = configurations.getProperty("CLOUD_USER");
                sqlPassword = configurations.getProperty("CLOUD_PASSWORD");

            } else {
                LOG.info("No Known Cluster Arguments Detected");
                System.exit(1);
            }

        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (ClassNotFoundException e) {
            LOG.error("SQLUtilities Main : CLass not found Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (IllegalAccessException e) {
            LOG.error("SQLUtilities Main : Illegal Access Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        } catch (InstantiationException e) {
            LOG.error("SQLUtilities Main : Instatiation Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
        Connection connection=null;

        try {
            connection = DriverManager.getConnection(sqlJDBC, sqlUser, sqlPassword);
        } catch (SQLException e) {
            LOG.error("SQLUtilities Main : SQL Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
        return connection;
    }

    public ArrayList<String> getMonitoringList(String confFilePath) {
        BasicConfigurator.configure();
        Properties configurations = new Properties();

        try {
            configurations.load(new FileInputStream(confFilePath));
        } catch (FileNotFoundException fnfe) {
            LOG.error("SQLUtilities Main : No valid configuration file path found ....");
            LOG.error(Arrays.toString(fnfe.getStackTrace()));
        } catch (IOException ioe) {
            LOG.error("SQLUtilities Main : IO Exception met ....");
            LOG.error(Arrays.toString(ioe.getStackTrace()));
        }

        ArrayList<String> dictionaryList = new ArrayList();

        String cloudSQLTblName = configurations.getProperty("CLOUD_DICT_TBL_NAME");
        String query = "SELECT * FROM " + cloudSQLTblName;
        try {
            Connection connection=new SQLUtilities()
                    .getSQLConnection("CLOUD_CLUSTER", confFilePath);
            Statement stmt=connection.createStatement();
            ResultSet resultSet = stmt.executeQuery(query);

            while (resultSet.next()) {
                String projectId = resultSet.getString(1);
                String flowId = resultSet.getString(2);

                String listString = projectId + "," + flowId;
                dictionaryList.add(listString);
            }
            resultSet.close();
            stmt.close();
            connection.close();
        } catch (SQLException e) {
            LOG.error("SQLUtilities Main : SQL Exception met ....");
            LOG.error(Arrays.toString(e.getStackTrace()));
        }
        return dictionaryList;
    }
}
