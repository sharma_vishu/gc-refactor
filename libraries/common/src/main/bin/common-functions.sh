#!/bin/bash
###############################################################################
#                               Documentation                                 #
###############################################################################
#                                                                             #
# Description                                                                 #
#     : This script consists of all the common utility functions.             #
#                                                                             #
# Note                                                                        #
#     : 1) If function argument ends with * then its required argument.       #
#       2) If function argument ends with ? then its optional argument.       #
#                                                                             #
###############################################################################
#                             Function Definitions                            #
###############################################################################


###
# Exit with given code
#
# Arguments:
#   exit_code*
#     - Exit code
#
function fn_exit(){

  exit_code=$1

  exit ${exit_code}

}


###
# Utility method to support unit testing to mock exit codes
# All functions should pass the exit code to this function and
# use the value return to check the exit code.
# Arguments:
#   exit_code*
#     - Exit code
#
function fn_get_exit_code(){

  exit_code=$1

  echo ${exit_code}

}

###
# Assert if given variable is set.
# If variable is,
#    Set - Do nothing
#    Empty - Exit with failure message
#
# Arguments:
#   variable_name*
#     - Name of the variable
#   variable_value*
#     - Value of the variable
#
function fn_assert_variable_is_set(){

  variable_name=$1

  variable_value=$2

  if [ "${variable_value}" == "" ]
  then

    exit_code=${EXIT_CODE_VARIABLE_NOT_SET}

    failure_messages="${variable_name} variable is not set"

    fn_exit_with_failure_message "${exit_code}" "${failure_messages}"

  fi

}

###
# Log given failure message and exit the script
# with given exit code
#
# Arguments:
#   exit_code*
#     - Exit code to be used to exit with
#   failure_message*
#     - Failure message to be logged
#
function fn_exit_with_failure_message(){

  exit_code=$1

  failure_message=$2

  fn_log_error "${failure_message}"

  fn_exit ${exit_code}

}


###
# Check the exit code.
# If exit code is,
#   Success - Log the success message and return
#   Failure - Log the failure message and exit with
#             the same exit code based on flag.
#
# Arguments:
#   exit_code*
#     - Exit code to be checked
#   success_message*
#     - Message to be logged if the exit code is zero
#   failure_message*
#     - Message to be logged if the exit code is non-zero
#   fail_on_error?
#     - If this flag is set then exit with the same exit code
#       Else write error message and return.
#
function fn_handle_exit_code(){

  exit_code=$1

  success_message=$2

  failure_message=$3

  fail_on_error=$4

  flow_name=$5

  table_name=$6

  if [ "${exit_code}" != "$EXIT_CODE_SUCCESS" ]
  then

    fn_log_error "${failure_message}"

    if [ "${fail_on_error}" != "$EXIT_CODE_SUCCESS" ]
    then
      fn_exit ${exit_code}
    fi

  else

    fn_log_info "${success_message}"

  fi

}


###
# Updating the custom modules in case of failed events
# Check the exit code.
# If exit code is,
#   Success - Log the success message and return
#   Failure - Log the failure message and exit with
#             the same exit code based on flag.
#
# Arguments:
#   exit_code*
#     - Exit code to be checked
#   success_message*
#     - Message to be logged if the exit code is zero
#   failure_message*
#     - Message to be logged if the exit code is non-zero
#   fail_on_error?
#     - If this flag is set then exit with the same exit code
#       Else write error message and return.
#

function fn_handle_exit_code_modules(){

  exit_code=$1

  success_message=$2

  failure_message=$3

  fail_on_error=$4

  flow_name=$5

  table_name=$6
   exit_code2=${exit_code}
  if [ "${exit_code}" != "$EXIT_CODE_SUCCESS" ]
  then

    fn_log_error "${failure_message}"

    if [ "${fail_on_error}" != "$EXIT_CODE_SUCCESS" ]
    then
      #Update the failed events in case of failed status
      JAR_PATH=${SHARED_LIB}/utility.jar
      config_file_path=${CONFIG_HOME}/${FLOW_NAME}"-"${SYNC_TABLE}"/sync-configurations.xml"
      java -cp ${JAR_PATH} com.utility.migrator.initializer.UpdateFailedJobs -f ${flow_name} -t ${table_name} -c ${config_file_path}
      fn_send_email ${EMAIL_ID} "Google-Cloud DataSync-Flow" "${failure_message}"
      fn_exit ${exit_code2}
    fi
  else
    fn_log_info "${success_message}"
  fi
}

###
# Check if the executable/command exists or not
#
# Arguments:
#   exit_code*
#     - Exit code to be checked
#   fail_on_error?
#     - If this flag is set then exit with the same exit code
#       Else write error message and return.
#
function fn_assert_executable_exists() {

  executable=$1

  fail_on_error=$2

  if ! type "${executable}" > /dev/null; then

    fn_log_error "Executable ${executable} does not exists"

    if [ "${fail_on_error}" == "${BOOLEAN_TRUE}" ];
    then

      fn_exit ${EXIT_CODE_EXECUTABLE_NOT_PRESENT}

    fi
  fi

}


function fn_assert_file_exists() {

  file_path="$1"

  fail_on_error=$2

  if [ ! -f "${file_path}" ]; then

    fn_log_error "File ${file_path} does not exists"

    if [ "${fail_on_error}" == "${BOOLEAN_TRUE}" ];
    then

      fn_exit ${EXIT_CODE_FILE_DOES_NOT_EXIST}

    fi
  fi

}

function fn_assert_file_not_empty(){

  file_to_be_checked="${1}"

  fn_assert_variable_is_set "file_to_be_checked" "${file_to_be_checked}"

  fn_assert_file_exists "${file_to_be_checked}" "${BOOLEAN_TRUE}"

  if [ ! -s "${file_to_be_checked}" ]; then

    fn_log_error "File ${file_to_be_checked} is empty"

    fn_exit ${EXIT_CODE_FILE_IS_EMPTY}

  fi

}

function fn_run_java(){

  fn_assert_executable_exists "java" "${BOOLEAN_TRUE}"

  module_home="${1}"

  fn_assert_variable_is_set "MODULE_HOME" "${module_home}"

  MODULE_CLASSPATH=""

  for i in $module_home/lib/*.jar; do
    MODULE_CLASSPATH=$MODULE_CLASSPATH:$i
  done

  MODULE_CLASSPATH=`echo $MODULE_CLASSPATH | cut -c2-`

  java -cp "${MODULE_CLASSPATH}" ${@:2}

  exit_code=`fn_get_exit_code $?`

  success_message="Successfully executed java command for module ${module_home} ${@:2}"

  failure_message="Failed to execute java command for module ${module_home} ${@:2}"

  fn_handle_exit_code "${exit_code}" "${success_message}" "${failure_message}" "${fail_on_error}"

}

function fn_chown_local_directory(){

  directory_path="$1"

  user_name="$2"

  recursively="$3"

  fail_on_error="$4"

  fn_assert_variable_is_set "directory_path" "${directory_path}"

  fn_assert_variable_is_set "user_name" "${user_name}"

  RECURSIVELY=""

  if [ "${recursively}" == "${BOOLEAN_TRUE}" ]; then

    RECURSIVELY="-R"

  fi

  chown ${RECURSIVELY} "${user_name}" "${directory_path}"

  exit_code=`fn_get_exit_code $?`

  success_message="Successfully changed ownership of directory ${directory_path} to ${user_name}"

  failure_message="Failed to change ownership of directory ${directory_path} to ${user_name}"

  fn_handle_exit_code "${exit_code}" "${success_message}" "${failure_message}" "${fail_on_error}"

}

function fn_create_local_directory(){

  directory_path="$1"

  fail_on_error="$2"

  fn_assert_variable_is_set "directory_path" "${directory_path}"

  mkdir -p "${directory_path}"

  exit_code=`fn_get_exit_code $?`

  success_message="Successfully created directory ${directory_path}"

  failure_message="Failed to create directory ${directory_path}"

  fn_handle_exit_code "${exit_code}" "${success_message}" "${failure_message}" "${fail_on_error}"

}



function fn_delete_recursive_local_directory(){

  directory_path="$1"

  fail_on_error="$2"

  fn_assert_variable_is_set "directory_path" "${directory_path}"

  rm -r "${directory_path}"

  exit_code=$?

  success_message="Successfully deleted directory ${directory_path}"

  failure_message="Failed to delete directory ${directory_path}"

  fn_handle_exit_code "${exit_code}" "${success_message}" "${failure_message}" "${fail_on_error}"

}


function fn_get_batch_id(){

    export BATCHID=$(date '+%Y%m%d%H%M%S')

}

function fn_delete_local_file(){

  file_name="$1"

  fail_on_error="$2"

  fn_assert_variable_is_set "file_name" "${file_name}"

  rm  "${file_name}"

  exit_code=$?

  success_message="Successfully deleted file ${file_name}"

  failure_message="Failed to delete file ${file_name}"

  fn_handle_exit_code "${exit_code}" "${success_message}" "${failure_message}" "${fail_on_error}"

}

function fn_batchid_file_exists() {

  file_name="$1"

  test -f "${file_name}"

  exit_code=$?

  if [ "${exit_code}" == "${EXIT_CODE_SUCCESS}" ];

  then

      failure_message="last execution ${file_name} file exists"

      fn_exit_with_failure_message "1" "${failure_message}"

  else

      test -f "${BATCH_ID_DATA_DIR}/new_${BATCH_ID_FILE_NAME}"

      exit_code=$?

          if [ "${exit_code}" != "${EXIT_CODE_SUCCESS}" ];

          then

              fn_create_local_directory "${BATCH_ID_DATA_DIR}"

              export BATCHID=$(date '+%Y%m%d%H%M%S')

              echo "${BATCHID}" > "${BATCH_ID_DATA_DIR}/new_${BATCH_ID_FILE_NAME}"

          else

              export BATCHID=$(cat "${BATCH_ID_DATA_DIR}/new_${BATCH_ID_FILE_NAME}")

          fi
  fi

}

function fn_mv_local_file(){

  source_file="$1"

  target_file="$2"

  fail_on_error="$3"

  fn_assert_variable_is_set "source_file" "${source_file}"

  fn_assert_variable_is_set "target_file" "${target_file}"

  mv "${source_file}" "${target_file}"

  exit_code=`fn_get_exit_code $?`

  success_message="Successfully moved the local directory ${source_file} to ${target_file}"

  failure_message="Failed to moved the local directory ${source_file} to ${target_file}"

  fn_handle_exit_code "${exit_code}" "${success_message}" "${failure_message}" "${fail_on_error}"


}


function fn_send_email(){

  email_recipients="$1"

  email_subject="$2"

  email_body="$3"

  email_attachment="$4"

  fail_on_error="$5"

  fn_assert_variable_is_set "email_recipients" "${email_recipients}"

  if [ "$#" == 4 ] && [ -f $email_attachment ]; then
    echo "${email_body}" | mail -s "${email_subject}"  -a "${email_attachment}" ${email_recipients}
  else
    echo "${email_body}" | mail -s "${email_subject}" ${email_recipients}
  fi

  exit_code=`fn_get_exit_code $?`

  success_message="Successfully send mail ${email_body} with subject ${email_subject} to ${email_recipients}  having attachement ${email_attachment} $#"

  failure_message="Failed to send mail ${email_body} with subject ${email_subject} to ${email_recipients}  having attachement ${email_attachment} $#"

  fn_handle_exit_code "${exit_code}" "${success_message}" "${failure_message}" "${fail_on_error}"


}


function fn_batchid_generate(){

  batch_id_file="$1"

  fail_on_error="$2"

  ## Export the batch_id value
  fn_get_batch_id

  echo "${BATCHID}" > "${batch_id_file}"

  exit_code=`fn_get_exit_code $?`

  success_message="Successfully created batch_id : ${BATCHID}"

  failure_message="Failed to create the batch_id : ${BATCHID}"

  fn_handle_exit_code "${exit_code}" "${success_message}" "${failure_message}" "${fail_on_error}"


}


function fn_export_batch_id() {

  file_path="$1"

  fail_on_error="$2"

  failure_message="File  ${file_path} does not exists and fail to export the batch_id"

  failure_message_export="Fail to export the batch_id"

  success_message="Successfully exported batch_id : ${BATCHID}"


  if [ -f $file_path ] && [ -s $file_path ];

  then
       export BATCHID=$(cat ${file_path})

       exit_code=$?

       fn_handle_exit_code "${exit_code}" "${success_message}" "${failure_message_export}" "${fail_on_error}"

  else
       fn_handle_exit_code "${EXIT_CODE_FAIL}" "${success_message}" "${failure_message}" "${fail_on_error}"

  fi

}

function validate_before_del(){
	filepath = $0
	if [ -d filepath ]
		then
		return 0
	else
		return -1
	fi
}

function fn_run_sql_utilities() {
    sql_file_path="$1"
    property_file_path=${CONFIG_HOME}/reair-configurations.properties

    java -cp ${SHARED_LIB}/utility.jar com.utility.migrator.initializer.RunSQLFile -f ${sql_file_path} -p ${property_file_path}
    fn_handle_exit_code "$?" "Successfully executed the setup script for SQL File = ${sql_file_path}" "Failed to execute ${sql_file_path}" "${fail_on_error}"
}

################################################################################
#                                     End                                      #
################################################################################