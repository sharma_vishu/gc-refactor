#!/bin/bash
###############################################################################
#                               Documentation                                 #
###############################################################################
#                                                                             #
# Description                                                                 #
#     : It consists of all the environment variables that user needs to       #
#       define                                                                #
#                                                                             #
#                                                                             #
#                                                                             #
###############################################################################
#                            Environment Variables                            #
###############################################################################

#Set this value to the kafka broker installation directory
#e.g. /usr/hdp/current/kafka-broker
export KAFKA_HOME=/usr/hdp/current/kafka-broker

#Set this value to zookeeper connection string used by Kafka
export KAFKA_ZOOKEEPER_CONNECTION_STRING=${BIGDATA_ZOOKEEPER}


export AZKABAN_TMP_DIR=${AZKABAN_TMP_DIR}

export LOCAL_DATA_DIR=${LOCAL_DATA_DIR}

export REDSHIFT_CREDENTIALS_FILE=${REDSHIFT_CREDENTIALS_FILE}













################################################################################
#                                     End                                      #
################################################################################