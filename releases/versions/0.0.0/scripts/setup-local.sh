#!/bin/bash
###############################################################################
#                               Documentation                                 #
###############################################################################
#                                                                             #
# Description                                                                 #
#     :                                                                       #
#                                                                             #
#                                                                             #
#                                                                             #
###############################################################################
#                        Setup HDFS Layer Directories                         #
###############################################################################

fn_create_local_directory  "${AZKABAN_TMP_DIR}"/of-flow-initializer

fn_create_local_directory  "${AZKABAN_TMP_DIR}"/of-schema-validator

fn_create_local_directory  "${AZKABAN_TMP_DIR}"/of-sync-utility

fn_create_local_directory  "${AZKABAN_TMP_DIR}"/of-bq-loader-utility

fn_create_local_directory  "${AZKABAN_TMP_DIR}"/of-s3-loader-utility

fn_create_local_directory  "${AZKABAN_TMP_DIR}"/datasync-migrational-transfer

fn_create_local_directory  "${AZKABAN_TMP_DIR}"/of-trigger-utility

################################################################################
#                                     End                                      #
################################################################################

