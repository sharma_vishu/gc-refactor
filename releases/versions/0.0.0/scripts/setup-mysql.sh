#!/bin/bash
###############################################################################
#                               Documentation                                 #
###############################################################################
#                                                                             #
# Description                                                                 #
#     :                                                                       #
#                                                                             #
#                                                                             #
#                                                                             #
###############################################################################
#                           Identify Script Home                              #
###############################################################################

fn_run_sql_utilities ${APPLICATION_INSTALL_DIR}/of-flow-initializer/etc/mysql-schema/ddh_persisted_flows.sql

fn_run_sql_utilities ${APPLICATION_INSTALL_DIR}/of-flow-initializer/etc/mysql-schema/failed_partitions.sql

fn_run_sql_utilities ${APPLICATION_INSTALL_DIR}/of-flow-initializer/etc/mysql-schema/flow_details.sql

fn_run_sql_utilities ${APPLICATION_INSTALL_DIR}/of-flow-initializer/etc/mysql-schema/job_dictionary.sql

fn_run_sql_utilities ${APPLICATION_INSTALL_DIR}/of-flow-initializer/etc/mysql-schema/partitions_metadata.sql

fn_run_sql_utilities ${APPLICATION_INSTALL_DIR}/of-flow-initializer/etc/mysql-schema/schema_details.sql

fn_run_sql_utilities ${APPLICATION_INSTALL_DIR}/of-flow-initializer/etc/mysql-schema/bq_dictionary.sql

fn_run_sql_utilities ${APPLICATION_INSTALL_DIR}/of-flow-initializer/etc/mysql-schema/bq_partitioned_data.sql

################################################################################
#                                     End                                      #
################################################################################
